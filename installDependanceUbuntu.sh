#!/bin/bash

#Est ce qu'on a les droits roots
if (( $EUID != 0 )); then
    echo "Vous devez lancer ce script avec les droits root."
    exit
fi

#Mise à jour des dépôts
apt update

#installation des dépendandances
apt -y install ffmpeg php7.0-cgi php-mbstring php-gd mpv mkvtoolnix

#installation de mplayer-toyunda
cd app/players/linux/
if [ ! -d "MPlayer-0.99X-Toyunda-lichking" ]; then
    echo "Vous devez extraire MPlayer-Toyunda à la main, dans le dossier app/players/linux"
    exit
fi
cd MPlayer-0.99X-Toyunda-lichking/
apt -y build-dep mpv
apt -y install gcc-4.9
./configure --cc=/usr/bin/gcc-4.9
make
make install
