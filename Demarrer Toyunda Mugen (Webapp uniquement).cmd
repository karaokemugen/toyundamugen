@echo off
cd /d %~dp0


if exist app\temp\webapp.pid (
    echo Webapp deja en cours d'execution. Quittez-la ou supprimez webapp.pid dans le dossier app\temp avant de relancer.
    pause
    exit /B
)

echo Lancement de la Webapp Toyunda Mugen.
echo Pause du script. L'appui sur une touche terminera la Webapp.

start "Toyunda-Haruka (Webapp)" /min php\php -S 0.0.0.0:1337 -t app
pause
REM On va poke la webapp pour la reveiller et lui faire cracher son PID
REM Au cas ou l'utilisateur n'est jamais alle dessus.
php\php.exe -f app\poke_webapp.php

set /p WebappPID=<app\temp\webapp.pid

REM Menage
del app\temp\webapp.pid


taskkill /F /PID %WebappPID%