
KARAS=`ls ini/*.ini`
while read KARA
do
	SUB=`cat "$KARA" | grep file | awk -F= {'print $2'}`	
	SUB=`echo "$SUB" | sed -e 's/^[ \t]*//'`
	SUB=`echo "$SUB" | sed -e 's/\r//'`
	if [ ! -e "lyrics/$SUB" ] && [ "$SUB" != "dummy.ass" ]
	then 
		echo "$SUB ($KARA)"
	fi
done <<< "$KARAS"
