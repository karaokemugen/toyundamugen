#!/bin/sh
cd kara
cp "../video/$1.webm" "Videos/$2.webm"
cp "../sub/$1.vtt" "Lyrics/$2.vtt"

INIFILE="./ini/$2.ini"

echo "origin=karaoke.moe" >$INIFILE
echo "[MOVIE]" >>$INIFILE
echo "directory=Videos" >>$INIFILE
echo "aviname=$2.webm" >>$INIFILE
echo "[SUBTITLES]" >>$INIFILE
echo "directory=Lyrics" >>$INIFILE
echo "file=$2.ass" >>$INIFILE

	

cd ..
