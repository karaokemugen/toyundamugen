EPOCH='jan 1 1970'
sum=0
rm sum.txt
for i in Videos/*
do
  TIME="00:00:00"
  rm info.txt >/dev/null
  ffmpeg -i "$i" >/dev/null 2>info.txt
  TIME=`cat info.txt | grep Duration | awk -F\  {'print $2'} | awk -F. {'print $1'} ` 
  sum="$(date -u -d "$EPOCH $TIME" +%s)"
  echo $sum >>sum.txt
  echo $i
done

echo $sum|/opt/bin/bc
date -u -d "jan 1 1970" +%s gives 0. So date -u -d "jan 1 1970 00:03:34" +%s
