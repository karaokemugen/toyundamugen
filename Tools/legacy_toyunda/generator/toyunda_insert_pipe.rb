#!/usr/bin/env ruby
##
## toyunda_insert_pipe.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Mar  5 19:31:12 2004 Olivier Leclant
## $Id: toyunda_insert_pipe.rb 1.4 Thu, 23 Sep 2004 01:51:27 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

# FIXME: recoder avec une vraie classe ToyundaLine de la ToyundaLib

if not ARGV.grep("--help").empty? then
  puts "Usage: #{$0} [lyrics_file...] [STRING]"
  puts "Also: #{$0} < lyrics_file [STRING]"
  puts "insert STRING at the begining of each subtitle line,"
  puts "STRING defaulting to '|'"
  exit
end
sep = ARGV.empty?? "|" : ARGV.pop

while line = gets do
  print line.sub(/(\{[0-9]*\}\{[0-9]*\})(.*)/, "\\1#{sep}\\2")
end
