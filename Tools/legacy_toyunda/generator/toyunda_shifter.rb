#!/usr/bin/env ruby
##
## toyunda_shifter.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Sat Apr  10 10:25:22 UTC 2004 Olivier Leclant
## $Id: toyunda_shifter.rb 1.5 Thu, 23 Sep 2004 17:49:00 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

require "toyunda-lib"

if ARGV.empty? then
  puts "Usage: #{$0} nbr lyrics";
  puts "Also:  #{$0} nbr < lyrics";
  exit!
end

num = ARGV.shift.to_i
file = ARGV.shift
input = file.nil? ? $stdin : File.open(file)

# FIXME: il est beurk, ce script, il est pas � jour du tout

line = input.gets
if line.chomp == ToyundaLib::ToyundaScripts::Identifier
  info "Rubix format detected"
  frm = ToyundaLib::FrmFile.new(file)
  frm.shift_by!(num)
  frm.write($stdout, true)
  exit
end

while line do
  tab = line.match(/\{([0-9]*)\}\{([0-9]*)\}(.*)/)
  if !tab.nil? then
    print "{", num + tab[1].to_i, "}{", num + tab[2].to_i, "}", tab[3], "\n"
  else
    print line
  end
  line = gets
end
