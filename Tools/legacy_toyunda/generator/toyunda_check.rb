#!/usr/bin/env ruby
##
## toyunda_check.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Mar  5 19:31:12 2004 Olivier Leclant
## $Id: toyunda_check.rb 1.4 Thu, 23 Sep 2004 01:51:27 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

#
# Checks that a file is a valid toyunda file
#

# FIXME: recoder dans la ToyundaLib

RegexpViolent = /^( *|[#-\/]*|\{(\d+)\}\{(\d+)\}(\|*(\{[csoCSO]:[^}]+\})*\|*)(.*))$/

RegexpGentil = /^( *|[#-\/].*| *\{(\d+)\}\{([\d ]*)\}(\|*(\{[csoCSO]:[^}]+\})*\|*)(.*))$/

for f in ARGV do
  File.open(f).each_line do |l|
    puts File.basename(f), l unless l.chomp.match RegexpGentil
  end
end
