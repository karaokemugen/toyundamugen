#!/usr/bin/env ruby
##
## toyunda.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Mar  5 22h environ 2004 Olivier Leclant
## $Id: toyunda.rb 1.8 Thu, 23 Sep 2004 17:49:00 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

#
# Reads file names from a .ini file, then launches a karaoke using
# mplayer-toyunda. Also reads playlists of .ini files. Can preprocess
# lyrics files with toyunda_insert_pipe.rb. Can guess partial files names.
#
# Bugs:
# Video and lyrics directory are constants, they not read from .ini
# files.
# It is complex to pass arguments to mplayer because we can't tell
# between a file or an argument which does not begin with '-'
#

module Toyunda
  
  def Toyunda.usage
    puts "Usage: #{$0} {file.ini,playlist,part of name} [MPLAYER_OPTIONS] [--amphi4=n]

--amphi4=n : specify a number of lines for shifting subtitles

Arguments order never matter.

mplayer-toyunda options must begin by '-', or they will be considered
filenames. You can use quotes to workaround like this: 
 $ #{$0} 'JAP - Nyo.ini' -fs '-vo help'"
    exit!
  end
  
  def Toyunda.main
    # ARGV parsing
    amphi4opts = ARGV.grep(/^--amphi4=/)
    @mplayer_opts = (ARGV.grep(/^-/) - amphi4opts).join(" ")
    files = ARGV.grep(/^[^-]/)
    usage if files.empty?
    @amphi4shift = amphi4opts.last.sub(/^--amphi4=/, "").to_i unless amphi4opts.empty?
    
    files.each do |filename|
      filename = File.expand_path(filename)
      if File.exists?(filename) then
        case filename
        when /.ini$/ then play_ini(filename)
        when /.m3u$/ then play_list(filename)
        when /.avi$/ then play_file(filename,
                                   guess_filename(filename.gsub(/.avi$/, ""), "txt"))
        when /.txt$/ then play_file(guess_filename(filename.gsub(/.txt$/, ""), "avi"),
                                    filename)
        when /list/ then play_list(filename)
        else play_list(filename)
        end
      else
        ini = guess_filename(filename, "ini")
        if not ini.nil? then
          play_ini(ini)
        else
          avi = guess_filename(filename, "avi")
          txt = guess_filename(filename, "txt") unless avi.nil?
          # on va se marrer si y'a plusieurs matches :)
          play_file(avi, txt)
        end
      end
    end
  end
  
  # j'encourage tout le monde � faire un lien symbolique ~/karaoke chez lui
  GuessRegexps = ["%s.",
                  "%s",
                  "~/karaoke/*%s*",
                  "~/karaoke/*/*%s*",
                  "*%s*",
                  "*/*%s*",
                  "*/*/*%s*",
                  "../*%s*",
                  "../*/*%s*",
                  "**/*%s*",
                  "../**/*%s*",
#                  "/**/*%s*",
  ] # moi �a me suffit, l�
  def Toyunda.guess_filename(filename, suffix)
    list = []
    GuessRegexps.each do |r|
      s = File.expand_path(r % File.basename(filename)) + suffix
      s = File.dirname(filename) + "/" + s unless s.match(/^[~\/]/)
      list = Dir[s]
      if list.empty? then
        puts "toyunda.rb: `#{s}' not found"
      else
        puts "toyunda.rb: found `#{list.join(",")}' matching #{File.basename(filename)}"
        return list[rand(list.size)] unless list.empty?
      end
    end
    puts "toyunda.rb: Could not find #{suffix} file `#{filename}'"
    return nil
  end
  
  def Toyunda.play_list(filename)
    File.open(filename).readlines.each do |ini|
# Cas du chemin absolu specifie dans un .ini !
      if (ini.match(/^\//)) then
        play_ini(ini.chomp)
      else
        play_ini(File.dirname(filename) + "/" + ini.chomp)
      end
    end
  end
  
  
  VideoDirectory = "/Videos/"
  LyricsDirectory = "/Lyrics/"
  def Toyunda.play_ini(filename)
    begin
      ini = File.open(filename).readlines
      
      # grep, get the file name, quote, add path
      video = ini.grep(/aviname.*=/i)
      video = video.first.sub(/.*=/, "").chomp
      video = video.gsub(/'/, "'\\\\''")
      video = File.dirname(filename) + VideoDirectory + video

      lyrics = ini.grep(/file.*=/i)
      lyrics = lyrics.first.sub(/.*=/, "").chomp
      lyrics = lyrics.gsub(/'/, "'\\\\''")
      lyrics_real = "/tmp/" +  lyrics
      lyrics = File.dirname(filename) + LyricsDirectory + lyrics
      
      if @amphi4shift.nil? then
        lyrics_real = lyrics
      else
        system "toyunda_insert_pipe.rb '#{"|" * @amphi4shift}'" +
                      " < '#{lyrics}' > '#{lyrics_real}'"
      end
      
      puts "===    inserted #{@amphi4shift} pipes" unless @amphi4shift.nil?
      puts "===    video=#{video}"
      puts "===    lyrics=#{lyrics}"
      
      return play_file(video, lyrics_real)
    rescue
      puts $!
      return false
    end
  end
  
  def Toyunda.play_file(video, sub)
    return false if video.nil? or sub.nil?
    begin
      cmd = "mplayer-toyunda -sub '#{sub}' '#{video}' #{@mplayer_opts}"
      puts "$ #{cmd}"
      if !system(cmd) then exit! end
      return true
    rescue
      puts $!
      return false
    end
  end
  
end

Toyunda.main
