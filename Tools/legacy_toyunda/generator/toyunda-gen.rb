#!/usr/bin/env ruby
##
## toyunda-gen.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Mar  5 12:02:40 2004 Olivier Leclant
## $Id: toyunda-gen.rb 1.7 Tue, 28 Sep 2004 20:21:23 +0200 olivier $
##
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

#
# Converts a frame number file and an annotated lyrics file into a
# toyunda lyrics file.
#

# Comment c'est fait : les donn�es d'entr�e sont les num�ros de frames
# (Frm) et les lyrics (Lyr). On les lit soit dans des fichiers
# stand-alone, soit dans les commentaires d'un .txt au format toyunda.
# le format des lyrics est �labor� et contient, en plus des lyrics
# avec s�parateurs de syllabes, tout le param�trage du comportement du
# script, sous forme de choix de style et de valeurs pour les
# variables.



Dir.chdir(File.dirname($0)) do
  require "variables"
  require "toyunda-lib"
  require "general"
end

require "singleton"

#include ToyundaLib

module ToyundaGen

  module Styles

    private
    module StandardCursor
      def output_cursor(writer, line)
        assert((not line.row.nil?), "@row is nil")

        return unless line.has_frames?

        writer.pipe_count = line.row + 1
        if line.vars.toyunda_char_fade_out then
          raise "toyunda_char_fade_out is Not ReImplemented Yet"
          # FIXME marche pu le toyunda_char_fade_out
          #  if i == @syls.size - 1 then fade = line.vars.fade_after else fade = @syls[i].stop - @syls[i].start end
          #  #puts    syl_line.add_colors_and_fade("FFFFFF", 0, 7) #fade)
          #end
        else
          for syl in line.syls do
            writer.add(syl.frames, line.size_string, line.only_cursor(syl)) if syl.has_frames?
          end
        end
        writer.pipe_count = line.row
      end
    end

    private
    class Style
      include Singleton
      include StandardCursor
      def init(vars); end
      def output(writer, line)
        if line.row.nil? then
          warn "no row number for line `#{line}'"
          return
        end

        # vertical placement
        writer.pipe_count = line.row

        # line callback
        process_line(writer, line) if self.respond_to? "process_line"

        # syls callback
        line.syls.each_index { |i| process_syl(writer, line, i) } if self.respond_to? "process_syl"

        # char callback
        (0...line.length).each { |i| process_char(writer, line, i) } if self.respond_to? "process_char"

        # cursor callback
        output_cursor(writer, line) if self.respond_to? "output_cursor" and not line.vars.no_cursors
      end
    end

    # FIXME: ajouter un truc genre 1 appel global au style avec toutes les lignes concern�es ?
    # FIXME: en fait, les m�thodes des Styles devraient renvoyer genre
    # un tableau [color, str, frames, (x,y)], comme �a chaque style
    # pourrait utiliser les autres pour faire une partie du boulot.
    # Sans parler du fait qu'on pourrait manipuler ce tableau de
    # mani�re plus facile que des appels de fonction.
    # Mais le writer.add est bien pratique...
    # Hmm... Suffirait de combiner les deux... en faisant renvoyer un ToyundaWriter par le style ! et en mettant un accesseur au contenu du writer
    # Skarsnik: NOTE: compl�tement :)
    private
    module SupportedStyles
      class Plain < Style
        def process_line(writer, line)
          if line.has_frames?
            writer.add(line.frames, line.colors[0], line)
          end
        end
      end

      class Progressive < Style
#        def process_syl(writer, line, i, fshift = i)
#          j = i
#          until line.syls[j].has_frames? or line.syls[j].nil? do j += 1 end
#          if not line.syls[j].nil? then
#            start, stop = line.syls[j].frames
#            str = line.only_syl(i)
#            writer.add([line.start - line.vars.fade_before + fshift, line.start + fshift], line.colors[0].fade_in, str)
#            writer.add([line.start + fshift, start], line.colors[0], str)
#            writer.add([start, stop], line.colors[1], str)
#            writer.add([stop, line.stop + fshift], line.colors[2], str)
#            writer.add([line.stop + fshift, line.stop + line.vars.fade_after + fshift], line.colors[2].fade_out, str)
#          end
#        end
        def process_char(writer, line, i, fshift = i)
          syl = line.syl_from_char(i)
          until line.syls[syl].has_frames? or line.syls[syl].nil? do syl += 1 end
          if not line.syls[syl].nil? then
            start, stop = line.syls[syl].frames
            str = line.only_char(i)
            writer.add([line.start - line.vars.fade_before + fshift, line.start + fshift], line.colors[0].fade_in, str)
            writer.add([line.start + fshift, start], line.colors[0], str)
            writer.add([start, stop], line.colors[1], str)
            writer.add([stop, line.stop + fshift], line.colors[2], str)
            writer.add([line.stop + fshift, line.stop + line.vars.fade_after + fshift], line.colors[2].fade_out, str)
          end
        end
      end

      class Mikomi < Style
        def process_char(writer, line, i)
          Progressive.instance.process_char(writer, line, i, 0)
        end
      end

    end

    private
    @@style_instances = Hash.new

    public
    def Styles.default_style_name
      SupportedStyles::Progressive.name.gsub(/.*::/, '').downcase
    end

    def Styles.get_all_styles_names
      SupportedStyles.constants.collect { |l| l.downcase }
    end

    def Styles.init_all_styles(vars)
      SupportedStyles.constants.each do |c|
        @@style_instances[c.downcase] = eval("SupportedStyles::" + c).instance
        @@style_instances[c.downcase].init(vars)
      end
    end

    def Styles.[](style_name)
      @@style_instances[style_name]
    end

    def Styles.support?(style_name)
      not (Styles[style_name]).nil?
    end

  end



  module Types
    private
    class Type
      attr_reader :value
      def initialize(value, doc = "")
        @doc = doc
        @default = value
        reset
      end
      def reset
        @value = @default
      end
      def default=(new_default)
        begin
          old = @value
          value = new_default # may raise an exception if new_default is invalid
          @value = old
        end
        @default = new_default
      end
      def to_s(var_name)
        "%s : %s = %s\t%s\n" % [var_name, self.class.to_s.gsub(/^.*::/, ""), @value.inspect, @doc]
      end
    end

    public
    class String < Type
      attr_writer :value
    end

    class Int < Type
      def value=(value)
        case value
        when /\d+/, Fixnum then @value = value.to_i
        else raise UserError, "invalid value `#{value}' for positive integer variable. Positive integer expected."
        end
      end
    end

    class PositiveInt < Type
      def value=(value)
        case value
        when /-?\d+/, Fixnum then @value = value.to_i
        else raise UserError, "invalid value `#{value}' for integer variable."
        end
      end
    end

    class Bool < Type
      def value=(value)
        case value
        when "true", true then @value = true
        when "false", false then @value = false
        else raise UserError, "invalid value `#{value}' for boolean variable. `true' or `false' expected."
        end
      end
    end

    class Color < Type
      def value=(value)
        case value
        when /^[0-9A-Fa-f]{6}$/ then @value = value
        else raise UserError, "invalid value `#{value}' for color variable. `[0-9A-Fa-f]{6}' expected."
        end
      end
    end

    class ColorArray < Type
      def value=(value)
        case value
        when /^[0-9A-Fa-f]{6}(,[0-9A-Fa-f]{6})*$/
          @value = []
          value.split(/,/).each do |v|
            @value << ToyundaColor.new(v)
          end
        else raise UserError, "invalid value `#{value}' for colors variable. `[0-9A-Fa-f]{6}(,[0-9A-Fa-f]{6})*' expected."
        end
      end
    end

    class Style < Type
      def value=(value)
        if Styles.support? value then
          info "using style `#{value}'"
          @value = value
        else
          warn "style not supported `#{value}': using default."
          @value = Styles.default_style_name
        end
      end
    end
  end



  module StartStopFramesAccessors
    def start
      assert(has_frames?, "in start accessor")
      frames[0]
    end

    def stop
      assert(has_frames?, "in stop accessor")
      frames[1]
    end

    attr_reader :frames

    NoCursor = nil

    def has_frames?
      return @frames != NoCursor
    end
  end


  class Syllable
    attr_reader :idx

    def initialize(str, frames, char_idx)
      @str = str
      @frames = frames
      @idx = char_idx
    end

    include StartStopFramesAccessors
    def to_s
      @str
    end
    def length
      to_s.length
    end
  end


  class Line
    private
    MaxLineLengthWarn = 38 # FIXME: make it vary on font size
    MaxWordLengthWarn = 11

    private
    def warn_user
      # display some warnings
      if length > MaxLineLengthWarn then
        warn "lyrics line too long (font will be resized!): `#{self}'"
      end
      to_s.split.each do |word|
        if word.length > MaxWordLengthWarn then
          warn "this word may be too long: `#{word}'\n  see if you can split it, it will be easier to read."
        end
      end
    end

    def initialize(syls_, frames_, vars_)
      assert(frames_.size == syls_.size, "in Line: %i /= %i" % [frames_.size, syls_.size])
      @vars = vars_.deep_clone
      @row = nil
      @font_size = vars.font_size

      # @colors
      raise UserError, "please choose a color in lyrics file" if [vars.color_before, vars.color_current, vars.color_after].include? nil
      @colors = [ToyundaLib::ToyundaColor.new(vars.color_before),
                 ToyundaLib::ToyundaColor.new(vars.color_current, vars.color_after),
                 ToyundaLib::ToyundaColor.new(vars.color_after)]

      # @syls
      char_idx = 0
      @syls = []
      for i in (0...syls_.size) do
        raise UserError, "empty syllable at line `#{line}'" if syls_[i].empty?
        @syls << Syllable.new(syls_[i], frames_[i], char_idx)
        char_idx += @syls[-1].length
      end

      # @frames
      if frames_[0] == NoCursor then # first syl without &
        if frames_[1].nil? then # no & at all on this line (fix this hack when syls with no frames can be parsed)
          @frames = NoCursor
        else # take the start frame of the 2nd syl
          @frames = [frames_[1][0], frames_[-1][1]]
        end
      else
        @frames = [frames_[0][0], frames_[-1][1]]
      end

      warn_user
    end

    public
    attr_reader :colors, :syls, :font_size, :vars, :row
    attr_writer :row

    def set_frames(frames_)
      assert((not frames_.nil? and frames_.size == 2), "in set_frames: invalid frames")
      @frames = frames_
    end

    include StartStopFramesAccessors
    def to_s
      return @syls.join
    end
    def to_toyunda_format
      return size_string + to_s
    end
    def [](i)
      to_s[i]
    end
    def length
      return to_s.length
    end
    def only_syl(i)
      return size_string + @syls[0...i].join('').gsub(/./, ' ') + @syls[i].to_s + @syls[i+1..-1].join('').gsub(/./, ' ')
    end
    def only_char(c)
      return size_string + (" " * c) + to_s[c..c] + (" " * (length - c - 1))
    end
    def only_cursor(s)
      syl = (s.is_a? Syllable) ? s : @syls[syl_from_char(s)]
      cursor_string = " " * length
      cursor_string[syl.idx] = ToyundaLib::ToyundaPlayer::CursorChar
      cursor_string = cursor_string[0..-2] if syl.length >= 2 and vars.center_on_syllable
      return cursor_string
    end
    def syl_from_char(i)
      len = 0
      syl = 0
      while (len <= i)
        len += @syls[syl].length
        syl += 1
      end
      syl - 1
    end
    def size_string # FIXME: font_size support is still gravement experimental
      if @font_size == ToyundaLib::ToyundaPlayer::DefaultFontSize then
        return ""
      else
        return "{s:#{@font_size}}"
      end
    end
  end


  class LineGroup < Array
    include StartStopFramesAccessors
    def frames
      check
      return [self.find { |line| line.has_frames? }.start,
              self.reverse.find { |line| line.has_frames? }.stop]
    end
    def has_frames?
      collect { |line| line.has_frames? }.include? true
    end

    def add_margins
      each do |line|
        if line.has_frames? then
          line.set_frames([self[0].start - vars.margin_before.to_i,
                           self[-1].stop + vars.margin_after.to_i])
        end
      end
    end

    def vars
      check; self[0].vars
    end
    def row
      check; self[0].row
    end
    def row=(row)
      each { |line| line.row = row }
    end

    private
    def check
      assert((has_frames? and not empty?), "in LineGroup: invalid LineGroup")
    end
  end




  # RowChooser's job is to choose which vertical positions will be
  # used by each LineGroup.
  class RowChooser
    include Singleton

    def initialize
      @managed_line_groups = []
    end

    public
    def manage(line_group)
      assert(line_group.has_frames?, "in RowChooser")

      if line_group.vars.force_row_position then
        row = line_group.vars.base_vertical_offset + line_group.vars.row_position
      else
        beg = line_group.start                                  # fading lines may superpose
        #beg = line_group.start - line_group.vars.fade_before   # fading lines may NOT superpose

        row = line_group.vars.base_vertical_offset
        until compatible?(row, beg, line_group.stop, line_group.vars)
          if line_group.vars.no_cursors then row += 1 else row += 2 end
        end
      end

      @managed_line_groups << line_group
      line_group.row = row
      # info "RowChooser attributed #{row} to `#{line_group}' (%i,%i)" % line_group.frames
    end

    private
    def compatible?(row, this, this_end, vars)
      @managed_line_groups.each do |line_group|
        next unless row == line_group.row
        s = line_group.start - vars.fade_before
        return false if this.between? s, (line_group.stop + vars.fade_after)
        return false if s.between? this, this_end
      end
      return true
    end
  end






  def ToyundaGen.parse_lyrics_file(lyr, frm, writer, vars)
    line_groups = []
    line_group = LineGroup.new
    grouping_lines = false
    frm_idx = 0

    lyr.each_line do |line|
      begin
        info line if /^%(set|reset|\{|\})/.match line # info pas d'un int�r�t flagrant.
        case line
        when /^%set\s+([^\s]+)\s+([^\s].*)$/ then vars[$1] = $2
        when /^%reset\s+([^\s]+)$/ then vars.reset($1)
        when /^%set_default\s+([^\s]+)\s+([^\s].*)$/ then vars.set_default($1, $2)
        when /^%color\s+([0-9A-Za-z]+)\s+([0-9A-Za-z]+)\s+([0-9A-Za-z]+)$/ # syntactic sugar for 3 %set with color decoding
          vars.color_before  = ToyundaLib::ColorName.to_bgr($1, vars.color_encoding)
          vars.color_current = ToyundaLib::ColorName.to_bgr($2, vars.color_encoding)
          vars.color_after   = ToyundaLib::ColorName.to_bgr($3, vars.color_encoding)
        when /^%\{$/
          raise UserError, "`%{': already in a %{" if grouping_lines
          grouping_lines = true
          line_group = LineGroup.new
        when /^%\}$/
          raise UserError, "`%}': not in a %{" unless grouping_lines
          grouping_lines = false
          line_groups << line_group
        when /^%style\s+(.*)$/ then vars.style = $1 # simple syntactic sugar
        when /^%raw\s+(.*)$/ then writer.add_raw! $1
        when /^%ignore_click\s+([0-9]+)$/ then frm.skip(frm_idx, frm_idx + $1.to_i)
        when /^%ignore_click\s+/ then raise UserError, "syntax error at line `#{line}'. Syntax is `%ignore_click number'"
        when /^%color\s+/ then raise UserError, "erroneous color line syntax : `#{line}'. Syntax is `%color xxxxxx xxxxxx xxxxxx'"
        when /^%set\s+/ then raise UserError, "syntax error at line `#{line}'. Syntax is `%set variable value'"
        when /^%reset\s+/ then raise UserError, "syntax error at line `#{line}'. Syntax is `%reset variable'"
        when /^%set_default\s+/ then raise UserError, "syntax error at line `#{line}'. Syntax is `%set_default variable value'"
        when /^%/ then warn "unsupported tag : `#{line}'. Upgrade to a newer version or correct the tag"
        else
          syls = line.split(/&/, -1)
          n = syls.size - 1
          if line.match(/^&/)
            frames = frm.frames[frm_idx...(frm_idx + n)]
            syls.delete_at(0)
          else
            frames = [Syllable::NoCursor] + frm.frames[frm_idx...(frm_idx + n)]
          end
          frm_idx += n

          if frames.size < syls.size then
            warn "not enough frame numbers available: adding fake clicks..."
            @fake_frame = frm.frames[-1][1] if @fake_frame.nil?
            frames << [@fake_frame + 1, @fake_frame + 5]
            @fake_frame += 5
          end
          lin = Line.new(syls, frames, vars)

          if grouping_lines then
            line_group << lin
          else
            warn "unused spaces on single line `#{line}'" if line.strip != line
            line_group = LineGroup.new(1, lin)
            line_groups << line_group
          end
        end
      rescue Variables::Error => e
        raise UserError, e
      rescue ToyundaLib::Error => e
        raise UserError, e
      end
    end
    raise UserError, "please close the `%{' !" if grouping_lines
    x = frm.frames.size - frm_idx
    warn "#{x} unused frame#{x == 1 ? "" : "s"} in FRM files." if x > 0
    return line_groups
  end

  def ToyundaGen.convert(out, lyrics_filename, frm_filename = lyrics_filename)
    begin
      lyr = ToyundaLib::LyricsFile.new(lyrics_filename)
      frm_orig = ToyundaLib::FrmFile.new(frm_filename)
      frm = frm_orig.clone
    rescue ToyundaLib::Error => e
      raise UserError, e
    end
    # FIXME: join_near! works on all the frms so Variable usage can't
    # be dynamic. I must recode join_near! and call it incrementally
    frm.join_near!(@vars.min_frames_between_syl).add_minimum_frame_length!


    out.puts ToyundaLib::ToyundaScripts::Identifier
    out.puts "{0}{0}{needed at beginning of file for file format recognition by mplayer}"

    lyr.dump(out)

    out.puts ToyundaLib::ToyundaScripts::GeneratedSubs
    writer = ToyundaLib::ToyundaWriter.new
    line_groups = ToyundaGen.parse_lyrics_file(lyr, frm, writer, @vars) # this also writes the %raw lines
    line_groups.each { |line_group| line_group.add_margins } # compute margins
    line_groups.each { |line_group| RowChooser.instance.manage(line_group) if line_group.has_frames? } # choose a vertical position
    line_groups.flatten.each { |line| Styles[line.vars.style].output(writer, line) } # output according to style
    writer.factorize!
    writer.dump(out)

    all_syls = line_groups.flatten.collect{ |l| l.syls.clone.delete_if{ |syl| not syl.has_frames? } }.flatten
    frm_orig.dump(out, all_syls)
  end

  def ToyundaGen.usage
    $stderr.puts "Usage: #{$0} LyricsFile [FrmFile]
--help                  get help and quit

Both .txt with comment and .lyr are ok for LyricsFile.
Both .txt with comment and .frm are ok for FrmFile.
FrmFile defaults to LyricsFile.

Options of lyrics file
----------------------
%{
%}
%color xxxxxx xxxxxx xxxxxx
%raw {0}{1}sub
%style {#{Styles.get_all_styles_names.join(",")}}
%set <variable> <value>
%reset <variable>
%set_default <variable> <value>
%ignore_click <number>

Variables of lyrics file
------------------------\n#{@vars}" # le \n c'est pour pas paumer l'indentation d'emacs
    exit!
  end

  def ToyundaGen.main
    chrono = Chrono.new

    # load all Styles
    Dir.chdir(File.dirname($0)) do
      Dir["plugins/*.rb"].each do |f|
        info "loading #{f}" # info pas d'un int�r�t flagrant.
        load f
      end
    end

    @vars = Variables.new
    # declare now all variables that are not Style-specific
    @vars.declare("min_frames_between_syl", Types::PositiveInt.new(5))
    @vars.declare("toyunda_char_fade_out", Types::Bool.new(false))
    @vars.declare("center_on_syllable", Types::Bool.new(true))
    @vars.declare("base_vertical_offset", Types::PositiveInt.new(0))
    @vars.declare("margin_before", Types::PositiveInt.new(35))
    @vars.declare("margin_after", Types::PositiveInt.new(0))
    @vars.declare("fade_before", Types::PositiveInt.new(10))
    @vars.declare("fade_after", Types::PositiveInt.new(5))
    @vars.declare("no_cursors", Types::Bool.new(false))
    @vars.declare("font_size", Types::PositiveInt.new(ToyundaLib::ToyundaPlayer::DefaultFontSize, "DO NOT USE. DOES NOT WORK YET."))
    @vars.declare("force_row_position", Types::Bool.new(false, "desactivate the RowChooser and use row_position"))
    @vars.declare("row_position", Types::PositiveInt.new(0))
    @vars.declare("color_encoding", Types::String.new("bgr", "choices are [rgb, bgr, human]"))
    @vars.declare("colors", Types::ColorArray.new(""))
    @vars.declare("style", Types::Style.new(Styles.default_style_name))
    @vars.declare("color_before", Types::Color.new(nil))
    @vars.declare("color_current", Types::Color.new(nil))
    @vars.declare("color_after", Types::Color.new(nil))

    # declare Style-specific variables
    Styles.init_all_styles(@vars)

    # we have all variables, we can show usage :)
    ToyundaGen.usage if ARGV.include? "--help" or ARGV.size > 2 or ARGV.empty?

    ToyundaGen.convert($stdout, *ARGV)
    info "---------- output generated in %.2f seconds ----------" % chrono.elapsed
  end

end

begin
  ToyundaGen.main
rescue UserError => error
  $stderr.puts("Error: " + error.to_s)
  exit!
end
