#!/usr/bin/env ruby
##
## frm_shifter.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Sat Apr  10 10:25:22 UTC 2004 Olivier Leclant
## $Id: frm_shifter.rb 1.3 Thu, 23 Sep 2004 03:00:22 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

require "toyunda-lib"

if ARGV.empty? or ARGV.size > 2 then
  puts "Usage: #{$0} nbr lyrics";
  puts "Also:  #{$0} nbr < lyrics";
  exit!
end

num = ARGV.shift.to_i
input = ARGV.shift

ToyundaLib::FrmFile.new(input).shift_by!(num).dump

# c'�tait vraiment tr�s compliqu�
