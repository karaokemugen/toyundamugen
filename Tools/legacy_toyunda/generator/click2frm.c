/*
** Copyright (C) 2003 Daniel Caujolle-Bert <segfault@club-internet.fr>
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**  
*/

/*
 * compile-command: "gcc -Wall -O2 `xine-config --cflags` `xine-config --libs` -L /usr/X11R6/lib -lX11 -lm -o click2frm click2frm.c"
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/extensions/XShm.h>

#include <xine.h>
#include <xine/xineutils.h>


#define CLICK2FRM_HELP "click2frm :\n\n\
 Invocation Possible : \n\
	click2frm [-p position] Video\n\
	-p Position, position est une valeur entre 0 et 65000\n\
	click2frm -p 32500  Video , va � la moiti� de la vid�o par exemple\n\n\
Commande disponnible :\n\
	 - : r�duis la vitesse\n\
	 + : augmente la vitesse\n\
	 Espace : Pause\n\
	 Fl�che droite : avance dans la vid�o\n\
	 Fl�che gauche : recule dans la vid�o\n\
	 k : affiche une ligne de --\n\n"
#define MWM_HINTS_DECORATIONS   (1L << 1)
#define PROP_MWM_HINTS_ELEMENTS 5
typedef struct {
  uint32_t  flags;
  uint32_t  functions;
  uint32_t  decorations;
  int32_t   input_mode;
  uint32_t  status;
} MWMHints;

static xine_t              *xine;
static xine_stream_t       *stream;
static xine_video_port_t   *vo_port;
static xine_audio_port_t   *ao_port;
static xine_event_queue_t  *event_queue;

static Display             *display;
static int                  screen;
static Window               window[2];
static int                  xpos, ypos, width, height, fullscreen;
static double               pixel_aspect;

static int                  running = 1;

/* this will be called by xine, if it wants to know the target size of a frame */
static void dest_size_cb(void *data, int video_width, int video_height, double video_pixel_aspect,
                         int *dest_width, int *dest_height, double *dest_pixel_aspect)  {
  *dest_width        = width;
  *dest_height       = height;
  *dest_pixel_aspect = pixel_aspect;
}

/* this will be called by xine when it's about to draw the frame */
static void frame_output_cb(void *data, int video_width, int video_height,
                            double video_pixel_aspect, int *dest_x, int *dest_y,
                            int *dest_width, int *dest_height, 
                            double *dest_pixel_aspect, int *win_x, int *win_y) {
  *dest_x            = 0;
  *dest_y            = 0;
  *win_x             = xpos;
  *win_y             = ypos;
  *dest_width        = width;
  *dest_height       = height;
  *dest_pixel_aspect = pixel_aspect;
}

static void event_listener(void *user_data, const xine_event_t *event) {
  switch(event->type) { 
  case XINE_EVENT_UI_PLAYBACK_FINISHED:
    running = 0;
    break;

  case XINE_EVENT_PROGRESS:
    {
      xine_progress_data_t *pevent = (xine_progress_data_t *) event->data;
      
      printf("%s [%d%%]\n", pevent->description, pevent->percent);
    }
    break;
  
  /* you can handle a lot of other interesting events here */
  }
}

int main(int argc, char **argv) {
  char              configfile[2048];
  x11_visual_t      vis;
  double            res_h, res_v;
  char             *vo_driver = "auto";
  char             *ao_driver = "auto";
  char             *mrl = NULL;
  int               i;
  /* Skarsnik begin */
  int	           spos = 0;
  double	    speed = 0;
  /* Skarsnik end*/
  Atom              XA_NO_BORDER;
  MWMHints          mwmhints;

  /* parsing command line */
  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-vo") == 0) {
      vo_driver = argv[++i];
    }
    else if (strcmp(argv[i], "-ao") == 0) {
      ao_driver = argv[++i];
    }
    /* Skarsnik begin */
    /* Add -p option for go to a position in stream at the begining */
    else if (strcmp(argv[i], "-p") == 0) {
      spos = atoi(argv[++i]);
    }
    else if (strcmp(argv[i], "--help") == 0) {
      printf(CLICK2FRM_HELP);
      return 1;
    }
    /* Skarsnik end */
    else
      mrl = argv[i];
  }

  if (!mrl) {
    printf("usage: %s [FILE] > file.frm  ,see --help for more info\n", argv[0]);
    return 1;
  }
  //  printf("mrl: '%s'\n", mrl);

  if (!XInitThreads()) {
    printf("XInitThreads() failed\n");
    return 1;
  }

  /* load xine config file and init xine */
  xine = xine_new();
  sprintf(configfile, "%s%s", xine_get_homedir(), "/.xine/config");
  xine_config_load(xine, configfile);
  xine_init(xine);
  
  display = XOpenDisplay(NULL);
  screen  = XDefaultScreen(display);
  xpos    = 0;
  ypos    = 0;
  width   = 320;
  height  = 200;

  /* some initalization for the X11 Window we will be showing video in */
  XLockDisplay(display);
  fullscreen = 0;
  window[0] = XCreateSimpleWindow(display, XDefaultRootWindow(display),
                                  xpos, ypos, width, height, 1, 0, 0);

  window[1] = XCreateSimpleWindow(display, XDefaultRootWindow(display),
                                  0, 0, (DisplayWidth(display, screen)),
                                  (DisplayHeight(display, screen)), 0, 0, 0);
  
  XSelectInput(display, window[0], (ExposureMask | ButtonPressMask | /* rubix begin */ ButtonReleaseMask | /* rubix end */ KeyPressMask |
                                    ButtonMotionMask | StructureNotifyMask | 
                                    PropertyChangeMask | PointerMotionMask));

  XSelectInput(display, window[1], (ExposureMask | ButtonPressMask | /* rubix begin */ ButtonReleaseMask | /* rubix end */ KeyPressMask |
                                    ButtonMotionMask | StructureNotifyMask | 
                                    PropertyChangeMask | PointerMotionMask));

  XA_NO_BORDER         = XInternAtom(display, "_MOTIF_WM_HINTS", False);
  mwmhints.flags       = MWM_HINTS_DECORATIONS;
  mwmhints.decorations = 0;
  XChangeProperty(display, window[1],
                  XA_NO_BORDER, XA_NO_BORDER, 32, PropModeReplace, (unsigned char *) &mwmhints,
                  PROP_MWM_HINTS_ELEMENTS);
  
  XMapRaised(display, window[fullscreen]);
  
  res_h = (DisplayWidth(display, screen) * 1000 / DisplayWidthMM(display, screen));
  res_v = (DisplayHeight(display, screen) * 1000 / DisplayHeightMM(display, screen));
  XSync(display, False);
  XUnlockDisplay(display);
  
  /* filling in the xine visual struct */
  vis.display           = display;
  vis.screen            = screen;
  vis.d                 = window[fullscreen];
  vis.dest_size_cb      = dest_size_cb;
  vis.frame_output_cb   = frame_output_cb;
  vis.user_data         = NULL;
  pixel_aspect          = res_v / res_h;
  
  /* opening xine output ports */
  vo_port = xine_open_video_driver(xine, vo_driver, XINE_VISUAL_TYPE_X11, (void *)&vis);
  ao_port = xine_open_audio_driver(xine , ao_driver, NULL);
  xine_post_init(xine, "strech", 1, ao_port, vo_port);
  /* open a xine stream connected to these ports */
  stream = xine_stream_new(xine, ao_port, vo_port);
  /* hook our event handler into the streams events */
  event_queue = xine_event_new_queue(stream);
  xine_event_create_listener_thread(event_queue, event_listener, NULL);
  
  /* make the video window visible to xine */
  xine_port_send_gui_data(vo_port, XINE_GUI_SEND_DRAWABLE_CHANGED, (void *) window[fullscreen]);
  xine_port_send_gui_data(vo_port, XINE_GUI_SEND_VIDEOWIN_VISIBLE, (void *) 1);

  
  /* start playback */
  if (!xine_open(stream, mrl) || !xine_play(stream, spos, 0)) {
    printf("Unable to open mrl '%s'\n", mrl);
    return 1;
  }
  
  /* rubix begin */
  int	duree_moisie = xine_get_stream_info(stream, XINE_STREAM_INFO_FRAME_DURATION);
  if (!duree_moisie)
    exit(42);
  /* rubix end */
  /* Skarsnik begin */
  #define XINE_FINE_SPEED_CURRENT xine_get_param(stream, XINE_PARAM_FINE_SPEED)
  speed = XINE_FINE_SPEED_CURRENT;
  /* Skarsnik end */
  while (running) {
    XEvent   xevent;
    
    XNextEvent(display, &xevent);
    
    switch(xevent.type) {
      /* rubix begin */
    case ButtonPress:
      {
	int moisi1;
	int moisi2;
	int moisi3;
	xine_get_pos_length(stream, &moisi1, &moisi2, &moisi3);
	printf("%i ", moisi2 * 90 / duree_moisie);
	fflush(stdout);
	break;
      }
    case ButtonRelease:
      {
	int moisi1;
	int moisi2;
	int moisi3;
	xine_get_pos_length(stream, &moisi1, &moisi2, &moisi3);
	printf("%i\n", moisi2 * 90 / duree_moisie);
	fflush(stdout);
	break;
      }
      /* rubix end */
    case KeyPress:
      {
        XKeyEvent  kevent;
        KeySym     ksym;
        char       kbuf[256];
        int        len;
        
        kevent = xevent.xkey;
        
        XLockDisplay(display);
        len = XLookupString(&kevent, kbuf, sizeof(kbuf), &ksym, NULL);
        XUnlockDisplay(display);
        
        switch (ksym) {
        
	/* Skarsnik Begin */
	/* Print the position when quit */
        case XK_q:
        case XK_Q:
	  {
	    int moisi1;
	    int moisi2;
	    int moisi3;
	
	    xine_get_pos_length(stream, &moisi1, &moisi2, &moisi3);
	    fprintf(stderr, "Pos : %d\n", moisi1);
	    running = 0;
	    break;
         } 
	/* Skarsnik end */
        case XK_f:
        case XK_F:
          {
            /* user pressed f => toggle fullscreen */
            Window    tmp_win;
            
            XLockDisplay(display);
            XUnmapWindow(display, window[fullscreen]);
            fullscreen = !fullscreen;
            XMapRaised(display, window[fullscreen]);
            XSync(display, False);
            XTranslateCoordinates(display, window[fullscreen],
                                  DefaultRootWindow(display),
                                  0, 0, &xpos, &ypos, &tmp_win);
            XUnlockDisplay(display);
            
            xine_port_send_gui_data(vo_port, XINE_GUI_SEND_DRAWABLE_CHANGED, 
                                    (void*) window[fullscreen]);
          }
          break;
        
        case XK_Up:
          /* cursor up => increase volume */
          xine_set_param(stream, XINE_PARAM_AUDIO_VOLUME,
                         (xine_get_param(stream, XINE_PARAM_AUDIO_VOLUME) + 1));
          break;
        /* Skarsnik begin */
	/* Implant basic stream seeking key*/
	case XK_Right:
	  {
	    int moisi1;
	    int moisi2;
	    int moisi3;
	  
	    xine_get_pos_length(stream, &moisi1, &moisi2, &moisi3);
	    if (xine_get_param(stream, XINE_PARAM_SPEED) == XINE_SPEED_PAUSE)
	      {
	        xine_play(stream, moisi1 + 800, 0);
	        xine_set_param(stream, XINE_PARAM_SPEED, XINE_SPEED_PAUSE);
	      }
	    else
	      {
	        xine_play(stream, moisi1 + 800, 0);
                xine_set_param(stream, XINE_PARAM_FINE_SPEED, speed);
	      }
	    break;
	  }
	 case XK_Left:
	  {
	    int moisi1;
	    int moisi2;
	    int moisi3;
	  
	    xine_get_pos_length(stream, &moisi1, &moisi2, &moisi3);
	    if (xine_get_param(stream, XINE_PARAM_SPEED) == XINE_SPEED_PAUSE)
	      {
	        xine_play(stream, moisi1 - 800, 0);
	        xine_set_param(stream, XINE_PARAM_SPEED, XINE_SPEED_PAUSE);
	      }
	    else
	      {
	        xine_play(stream, moisi1 - 800, 0);
                xine_set_param(stream, XINE_PARAM_FINE_SPEED, speed);
	      }
	    break;
	  }
	/* Skarsnik end */
        case XK_Down:
          /* cursor down => decrease volume */
          xine_set_param(stream, XINE_PARAM_AUDIO_VOLUME,
                         (xine_get_param(stream, XINE_PARAM_AUDIO_VOLUME) - 1));
          break;
        
	/* Skarsnik Begin */ 
        case XK_plus:
	  /* Slow down video */
          xine_set_param(stream, XINE_PARAM_FINE_SPEED, \
	  XINE_FINE_SPEED_CURRENT + XINE_FINE_SPEED_NORMAL / 10);
	  speed = XINE_FINE_SPEED_CURRENT;
          break;
        
        case XK_k:
          printf("-----\n");
	  break;
        case XK_minus:
	  /* Speed up video */
	  xine_set_param(stream, XINE_PARAM_FINE_SPEED, \
	  XINE_FINE_SPEED_CURRENT - XINE_FINE_SPEED_NORMAL / 10);
	  speed = XINE_FINE_SPEED_CURRENT;
	  break;
	
        case XK_space:
          /* space => toggle pause mode */
          if (xine_get_param(stream, XINE_PARAM_SPEED) == XINE_SPEED_PAUSE)
            xine_set_param(stream, XINE_PARAM_FINE_SPEED, speed);
          else
            xine_set_param(stream, XINE_PARAM_FINE_SPEED, XINE_SPEED_PAUSE);
          break;
	/* SKarsnik end */
	
        }
      }
      break;
      
    case Expose:
      /* this handles (partial) occlusion of our video window */
      if (xevent.xexpose.count != 0)
        break;
      xine_port_send_gui_data(vo_port, XINE_GUI_SEND_EXPOSE_EVENT, &xevent);
      break;
      
    case ConfigureNotify:
      {
        XConfigureEvent *cev = (XConfigureEvent *) &xevent;
        Window           tmp_win;
        
        width  = cev->width;
        height = cev->height;
        
        if ((cev->x == 0) && (cev->y == 0)) {
          XLockDisplay(display);
          XTranslateCoordinates(display, cev->window,
                                DefaultRootWindow(cev->display),
                                0, 0, &xpos, &ypos, &tmp_win);
          XUnlockDisplay(display);
        } else {
          xpos = cev->x;
          ypos = cev->y;
        }
      }
      break;
    
    }
  }
  
  /* cleanup */
  xine_close(stream);
  xine_event_dispose_queue(event_queue);
  xine_dispose(stream);
  xine_close_audio_driver(xine, ao_port);  
  xine_close_video_driver(xine, vo_port);  
  xine_exit(xine);
  
  XLockDisplay(display);
  XUnmapWindow(display, window[fullscreen]);
  XDestroyWindow(display, window[0]);
  XDestroyWindow(display, window[1]);
  XUnlockDisplay(display);
  
  XCloseDisplay (display);
  
  return 0;
}
