class AviFile

  attr_reader :filename, :period, :frames, :streams, :width, :height, :vids, :auds

  @@codecs = {
    "3IV0" => "3IVX", "3IV1" => "3IVX", "3IV2" => "3IVX",
    "AASC" => "Autodesk", "AFLI" => "Autodesk", "AFLC" => "Autodesk",
    "AP41" => "Angelpotion",
    "CRAM" => "Video 1", "MSVC" => "Video 1",
    "DIV3" => "DivX 3 low-motion", "DIV4" => "DivX 3 fast-motion",
    "DIVX" => "DivX 4", "DX50" => "DivX 5",
    "MJPG" => "Motion JPEG",
    "MP41" => "Microsoft MPEG 4 V1", "MP42" => "Microsoft MPEG 4 V2",
    "MP43" => "Microsoft MPEG 4 V3",

    "CVID" => "Radius Cinepak", "HFYU" => "Huffyuv 2.1.1",
    "I420" => "Intel 4:2:0 Video V2.50", "IV32" => "Indeo 3.2",
    "IV41" => "Indeo 4.5", "IV50" => "Indeo 5.10",
    "IYUV" => "Intel IYUV", "M261" => "Microsoft H.261",
    "M263" => "Microsoft H.263", "MRLE" => "Microsoft RLE",
    "MSVC" => "Video 1", "VIFP" => "VFAPI Wrapper codec",
    "XVID" => "XviD MPEG-4"
  }

  @@audiocodecs = {
    "\x02\x00" => "ADPCM",
    "\x11\x00" => "IMA ADPCM",
    "\x06\x00" => "CCITT G.711",
    "\x07\x00" => "CCITT G.711",
    "\x31\x00" => "Microsoft GSM 6.10",
    "\x22\x00" => "TrueSpeech",
    "\x42\x00" => "Microsoft G.723.1",
    "\x60\01x" => "WMA / DivX Audio",
    "\x61\x01" => "WMA / DivX Audio",
    "\x30\x01" => "Sipro",
    "\x02\x04" => "Indeo audio",
    "\x55\x00" => "Fraunhofer MP3",
    "\x00\x20" => "AC3",
    "\x4F\x67" => "Ogg Vorbis"
  }

  def initialize(filename)
    file = File.open(filename)
    @next_is_audio = false
    @filename = filename
    buffer = file.read 4
    raise "Not an AVI file" if buffer != "RIFF"
    buffer = file.read 4 # Read and discard the size
    buffer = file.read 4
    raise "Not an AVI file" if buffer != "AVI "
    read_chunk(file)
    file.close
  end

  def to_n(s)
    s[0] + 2**8 * s[1] + 2**16 * s[2] + 2**24 * s[3]
  end

  def read_chunk(file)
    fourcc = file.read 4
    case fourcc
    when "LIST"
      parse_list(file)
    else
      parse_chunk(fourcc, file)
    end
  end

  def parse_list(file)
    size = to_n(file.read 4)
    id = file.read 4
    sizeread = 0
    while size > sizeread
      sizeread += read_chunk(file)
    end
    return size + 8
  end

  def parse_chunk(fourcc, file)
    size = to_n(file.read 4)
    if 1 == size % 2 then size += 1 end
    case fourcc
    when "avih"
      @period = to_n(file.read 4)
      file.read 12
      @frames = to_n(file.read 4)
      file.read 4
      @streams = to_n(file.read 4)
      file.read 4
      @width = to_n(file.read 4)
      @height = to_n(file.read 4)
      file.read 16
    when "strh"
      @next_is_audio = false
      case file.read 4
      when "vids"
        @vids = file.read 4
      when "auds"
        @next_is_audio = true
        file.read 4
      end
      file.seek(size - 8, 1)
    when "strf"
      if (@next_is_audio)
        @auds = file.read 2
        file.seek size - 2, 1
      else
        file.seek size, 1
      end
    else
      file.seek size, 1
    end
    return size + 8
  end

  def fps
    return 1_000_000.0 / @period
  end

  def duration
    return @frames * @period / 1_000_000
  end

  def resolution
    return "#{@width}x#{@height}"
  end

  def codec
    codec = @@codecs[@vids.upcase]
    return codec if codec
    return @vids
  end

  def audio_codec
    codec = @@audiocodecs[@auds]
    return codec if codec
    return @auds.dump
  end

end
