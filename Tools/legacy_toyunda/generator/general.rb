##
## general.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Wed Aug 25 22:41:38 2004 Olivier Leclant
## $Id: general.rb 1.4 Tue, 28 Sep 2004 20:21:23 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

# General-purpose lib

# ancestor for all classes that represent a file.
# implements some IO syntactic sugar.
class FileClass
  def load(input, *rest)
    case input
    when String then input = File.open(input)
    when IO
    when nil then input = $stdin
    else raise TypeError, "bad type for input"
    end
    load_real(input, *rest)
    self
  end
  
  def save(out, *rest)
    case out
    when String then out = File.open(out, "w")
    when IO
    when nil then out = $stdout
    else raise TypeError, "bad type for output"
    end
    save_real(out, *rest)
    self
  end
  
  def dump(out = $stdout, *rest)
    save(out, *rest)
  end
end


# chronometer
require "date"
class Chrono
  def initialize
    start!
  end
  def start!
    @start = DateTime.now
    @stop = nil
    self
  end
  def stop!
    @stop = DateTime.now
    self
  end
  def elapsed_seconds
    elapsed = if @stop.nil? then DateTime.now - @start else @stop - @start end
    return (elapsed * 3600 * 24).to_f
  end
  alias elapsed elapsed_seconds
end



def info(*msg)
  $stderr.puts "Info: " + msg.to_s
end

def warn(*msg)
  $stderr.puts "Warning: " + msg.to_s
end

def debug(*msg)
  $stderr.puts "Debug: " + msg.to_s
end

class UserError < Exception; end
class AssertionError < Exception; end

def assert(test, msg)
  return if test
  raise AssertionError, msg
end

