##
## test.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Thu Sep 23 00:52:37 2004 Olivier Leclant
## $Id: test.rb 1.3 Thu, 23 Sep 2004 17:49:00 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

# regression tests for toyunda_tools

$res = 0

def test(b, s)
  puts "=== TEST #{s} ==="
  if b then puts "=== PASSED #{s} ===" else puts "=== FAILED #{s} ==="; $res = 1 end
end

def zsh(cmd)
  cmd.gsub!(/\\/, "\\\\")
  cmd.gsub!(/'/, "'\\\\''")
  puts("zsh -c '#{cmd}'")
  system("zsh -c '#{cmd}'")
end

txt = "'test/Polon - OP.txt'"

test(File.exists?(txt[1..-2]), txt)

test(zsh("cmp =(./frm_shifter.rb 0 #{txt} | sort | grep -v '^#') =(./toyunda2frm.rb #{txt} | sed 's/^/==/' | sort)"), "frm_shifter")

test(zsh("cmp =(./toyunda_refactorizer.rb #{txt}) =(./toyunda_refactorizer.rb =(./toyunda_refactorizer.rb #{txt}))"), "toyunda_refactorizer")

test(zsh('cmp test/Polon\ -\ OP.txt =(./toyunda-gen.rb 2>/dev/null test/Polon\ -\ OP.txt)'), "toyunda-gen")


exit $res
