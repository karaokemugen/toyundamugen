##
## frm_merge.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Wed Jul 14 14:40:43 2004 Olivier Leclant
## $Id: frm_merge.rb 1.4 Thu, 23 Sep 2004 03:00:22 +0200 olivier $
## 

## FIXME: do this job directly in the FrmFile class

require "toyunda-lib"

include ToyundaLib

def load_frms(files)
  frms = Array.new
  len = nil
  files.each do |filename|
    $stderr.print "Loading #{filename}..."
    frm = FrmFile.new(filename)
    $stderr.puts " #{frm.number_of_clicks} clicks found."
    if len.nil?
      len = frm.number_of_clicks
    elsif len != frm.number_of_clicks
      error "number of clicks does not match in all FRMs."
    end
    frms << frm
  end
  return frms, len
end


def moyenne(frms, len)
  bmoy = Array.new(len, 0)
  emoy = Array.new(len, 0)
  frms.each do |frm|
    for i in (0...len)
      bmoy[i] += frm.frames_beg[i].to_i
      emoy[i] += frm.frames_end[i].to_i
    end
  end
  bmoy.map! do |elt| (elt.to_f / frms[0].number_of_clicks).to_i end
  emoy.map! do |elt| (elt.to_f / frms[0].number_of_clicks).to_i end
  return bmoy, emoy
end


def moyenne_ponderee(frms, len, bmoy, emoy)
  bm = Array.new(len, 0)
  em = Array.new(len, 0)
  bs = Array.new(len, 0)
  es = Array.new(len, 0)
  frms.each do |frm|
    for i in (0...len)
      bcoef = (1.0 / 1 + (frm.frames_beg[i].to_i - bmoy[i]).abs)
      ecoef = (1.0 / 1 + (frm.frames_end[i].to_i - emoy[i]).abs)
      bm[i] += frm.frames_beg[i].to_i * bcoef
      em[i] += frm.frames_end[i].to_i * ecoef
      bs[i] += bcoef
      es[i] += ecoef
    end
  end
  bm.each_index do |i| bm[i] = (bm[i].to_f / bs[i]).to_i end
  em.each_index do |i| em[i] = (em[i].to_f / es[i]).to_i end
  return bm, em
end


def usage
  puts "usage: #{$0} [FRMFILES...]"
  exit!
end


usage if ARGV.nil? or ARGV.include? "--help"
frms, len = load_frms(ARGV)
bmoy, emoy = moyenne(frms, len)
b, e = moyenne_ponderee(frms, len, bmoy, emoy)


# write frm
for i in (0...len) do
  puts "#{b[i]} #{e[i]}"
end

