##
## wave.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:11:03 2004 Olivier Leclant
## $Id: wave.rb 1.3 Mon, 27 Sep 2004 11:15:36 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

require "toyunda-lib"

module ToyundaGen
  module Styles
    module SupportedStyles
      class Wave < Style
        def init(vars)
          vars.declare("wave_before", Types::PositiveInt.new(80))
          vars.declare("fade_during", Types::PositiveInt.new(vars.wave_before - 40))
          vars.declare("amplitude", Types::PositiveInt.new(100))
          vars.declare("oscillations", Types::PositiveInt.new(8)) # must be div by 4
        end
        def borne(val, min, max)
          val = [val, min].max
          val = [val, max].min
          return val
        end
        def process_char(writer, line, i)
          wave_before = line.vars.wave_before
          fade_during = line.vars.fade_during
          oscillations = line.vars.oscillations
          amplitude = line.vars.amplitude
          
          str = line.only_char(i)
          initial = line.start - wave_before
          fade_end = line.start - wave_before + fade_during
          wave_end = line.start
          
          initial.upto(wave_end + line.length) do |frame|
            idx = (frame - initial - i).to_f
            visibility =  idx / fade_during.to_f # 0 % at initial, 100 % at fade_end
            visibility = (255 * visibility).to_i
            visibility = borne(visibility, 0, 255)
            
            angle = oscillations * Math::PI * (idx / wave_before.to_f)
            angle = borne(angle, 0, oscillations * Math::PI)
            if angle != 0 then y = (amplitude * Math.sin(angle) / angle).to_i else y = 0 end
            
            x = ToyundaLib::Conversion.length_to_x(line.length) + (10 * Math.sin(angle / 4)).to_i
            
            writer.add([frame - 1, frame], "{c:$%02X#{line.colors[0].bgr}}{o:#{x},#{y}}" % visibility, str)
          end
        end
        def process_syl(writer, line, i)
          syl = line.syls[i]
          if syl.has_frames? then
            position = "{o:#{ToyundaLib::Conversion.length_to_x(line.length)},0}"
            writer.add([line.start + line.length, syl.start], line.colors[0], position, line.only_syl(i))
            writer.add([syl.stop, line.stop + i], line.colors[2], position, line.only_syl(i))
            writer.add([syl.start, syl.stop], line.colors[1], position, line.only_syl(i))
            writer.add([line.stop + i, line.stop + line.vars.fade_after + i], line.colors[2].fade_out, position, line.only_syl(i))
          end
        end
      end
    end
  end
end
