##
## unfold.rb.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id: unfold.rb 1.1 Mon, 27 Sep 2004 11:15:36 +0200 olivier $
##
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles
      class Unfolding < Style
        def init(vars)
          vars.declare("unfold_shift_each_char", Types::Bool.new(false))
        end

        def process_char(writer, line, i)
          syl = line.syl_from_char(i)
          fshift = line.vars.unfold_shift_each_char ? i : 0
          origin = ToyundaLib::Conversion.length_to_x(line.length)
          until line.syls[syl].has_frames? or line.syls[syl].nil? do syl += 1 end
          if not line.syls[syl].nil? then
            start, stop = line.syls[syl].frames
            str = line[i..i]
            c = ToyundaLib::Conversion.char_width(line.font_size.to_i)
            
            writer.add([line.start - line.vars.fade_before - line.vars.margin_before, line.start - line.vars.margin_before + fshift], line.colors[0].fade_in,
                       "{o:#{origin},0:#{origin + (c*i).to_i},0}", str)
            writer.add([line.start - line.vars.margin_before + fshift, start], line.colors[0],
                       "{o:#{origin + (c*i).to_i},0}", str)
            writer.add([start, stop], line.colors[1],
                       "{o:#{origin + (c*i).to_i},0}", str)
            writer.add([stop, line.stop + fshift], line.colors[2],
                       "{o:#{origin + (c*i).to_i},0}", str)
            writer.add([line.stop + fshift, line.stop + line.vars.fade_after + line.vars.margin_after + fshift], line.colors[2].fade_out,
                       "{o:#{origin + (c*i).to_i},0:#{origin + (c*line.length).to_i},0}", str)
          end
        end
      end
    end
  end
end
