#!/usr/bin/env ruby
##
## dwi2toyunda.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Mar  5 12:02:40 2004 Olivier Leclant
## $Id: dwi2toyunda.rb 1.1 Thu, 11 Mar 2004 15:07:39 +0100 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

#
# Convert a DWI file to the DDR extension of toyunda lyrics format
# for mplayer-toyunda.
#
# This essentially means parsing and translation from beat count to frame
# count.
#
# FIXME: BPM changes are hardcoded for "White Reflection.dwi"


# class for storing the freezing state of the arrows during parse
# "7!70009" means "freeze on up and left...
# then, release up and press right arrow"
class FreezeState

  ReleaseUp = {?7 => ?4,
               ?8 => ?0,
               ?9 => ?6,
               ?A => ?2}

  ReleaseDown = {?1 => ?4,
                 ?2 => ?0,
                 ?3 => ?6,
                 ?A => ?8}

  ReleaseLeft = {?7 => ?8,
                 ?4 => ?0,
                 ?1 => ?2,
                 ?B => ?6}

  ReleaseRight = {?9 => ?8,
                  ?6 => ?0,
                  ?3 => ?2,
                  ?B => ?4}

  def initialize
    @up = @down = @left = @right = false
  end

  # called when "!" is read
  def hold(c, state = true)
    @up = state unless not ReleaseUp.keys.include?(c)
    @down = state unless not ReleaseDown.keys.include?(c)
    @left = state unless not ReleaseLeft.keys.include?(c)
    @right = state unless not ReleaseRight.keys.include?(c)
  end
  
  # called when any step is read
  def process(c)
    if @up and ReleaseUp[c] != nil then
      @up = false;
      c = ReleaseUp[c]
    end
    if @down and ReleaseDown[c] != nil then
      @down = false;
      c = ReleaseDown[c]
    end
    if @left and ReleaseLeft[c] != nil then
      @left = false;
      c = ReleaseLeft[c]
    end
    if @right and ReleaseRight[c] != nil then
      @right = false;
      c = ReleaseRight[c]
    end
    return c
  end
end


class DWI2Toyunda

  ToyundaStrings = {?1 => "{d:<v}",
                    ?2 => "{d:v}",
                    ?3 => "{d:v>}",
                    ?4 => "{d:<}",
                    ?6 => "{d:>}",
                    ?7 => "{d:<^}",
                    ?8 => "{d:^}",
                    ?9 => "{d:^>}",
                    ?A => "{d:^v}",
                    ?B => "{d:<>}",
  }
  
  ScrollingSpeed = 7000.0
  
  def print_toyunda_line(c, out_file)
    return if c == ?0
    raise "internal error" if c == nil or ToyundaStrings[c] == nil
    out_file.print "{", (@frame - ScrollingSpeed / @cur_bpm.to_i).to_i, "}"
    out_file.print "{", @frame.to_i, "}"
    out_file.print ToyundaStrings[c], "\n"
  end


  # beware, for the formula of the frame number is subtle, and quick
  # to merdouille

  # tsukurimashou...
  FPS = 30
  X = -52
  Y = 4 * FPS
  
  def next_frame(speed)
    @beat += speed

    # -------------------- Don't look here ---------------------------
    # static formula (without BPM change) is
    # frame = X + (Y * GAP / 1000) + (Y * 60 * BEAT / 8 / BPM);
    # hardwired for BPM changes in "Two Mix - White Reflection.dwi"
    # FIXME: read changeBPMs from the DWI file and make them work all right
    if @beat >= 144 / 2 then @cur_bpm = 145 end
    if @beat >= 1552 / 2 then @cur_bpm = 290 end
    if @beat >= 2196 / 2 then
      @cur_bpm = 145
      begin @frame += 7; $vieux_flag_moisi = true end unless $vieux_flag_moisi
    end
    # ----------------------------------------------------------------

    # incremental update
    @frame += (Y * 60 * speed.to_f / 8.0 / @cur_bpm.to_f)
  end



  SpeedTable = {?( => 1.0 / 2,
                ?[ => 1.0 / 3,
                ?{ => 1.0 / 8,
                ?` => 1.0 / 24,
                ?) => 1.0,
                ?] => 1.0,
                ?} => 1.0,
                ?' => 1.0,
               }

  # freeze_state stores which directions are freezed
  # speed is the number of beats between a step and the next
  # cur_bpm is the current BPM (can be modified due to changeBPMs)
  # frame is the current frame number for mplayer-toyunda
  def read_steps(file, out_file)
    speed = 1
    freeze_state = FreezeState.new
    $vieux_flag_moisi = false
    @cur_bpm = @header["BPM"]
    @frame = X + Y * (@header["GAP"].to_f / 1000.0)
    @beat = 0.0

    until ((c = file.readchar) == ?;)
      case c
        when ?\n, ?\r then
          # non, rien.
        when ?/ then
          out_file.print "// DWI comment /", file.readline
        when ?(, ?[, ?{, ?`, ?), ?], ?}, ?' then
          speed = SpeedTable[c]
        when ?! then
          freeze_state.hold(file.readchar)
        when ?0, ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?A, ?B then
          c = freeze_state.process(c)
          print_toyunda_line(c, out_file)
          next_frame(speed)
        else
          out_file.puts "// unsupported character `#{c.chr}'"
      end
    end
  end



  def initialize(filename)
    @header = Hash.new

    # first-pass, read all header lines
    File.open filename do |file|
      file.each_line do |line|
        r = line.match(/^#(.*):(.*);/);
        @header[r[1]] = r[2] unless r == nil
      end
    end
    
    # really translate steps in each mode
    File.open filename do |file|
      while true
        begin
          until (line = file.readline).match(/#SINGLE/)
          end
        rescue
          break
        end
        r = line.match(/#SINGLE:(.*):(.*):/)
        # FIXME: we will have a parsing problem
        # if the steps begins on the `#SINGLE' line
        
        File.open(filename + "." + r[1] + ".txt", "w") do |out_file|
          puts "writing #{out_file.path}..."
          out_file.puts "///////////////////////////////////////////////////"
          out_file.puts "// This file was generated by Rubix's dwi2toyunda !"
          for s in ["TITLE","ARTIST","GENRE","BPM","GAP"]
            out_file.puts "// #{s} = #{@header[s]}"
          end
          out_file.puts "// mode = #{r[1]}"
          out_file.puts "// diff = #{r[2]}"
          read_steps(file, out_file)
        end
      end
    end
    if @header.size < 5 then
      puts "To me, `#{filename}' did not look like a valid DWI file."
    else
      puts "`#{filename}' processed succesfully !"
    end
  end

end


# main
require "English"

if ARGV.empty? or ! ARGV.grep("--help").empty? then
  puts "Usage: #{$PROGRAM_NAME} [FILE]..."
  exit!
end

for filename in ARGV
  DWI2Toyunda.new(filename)
end
