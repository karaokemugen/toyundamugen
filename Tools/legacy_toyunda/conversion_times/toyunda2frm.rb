#!/usr/bin/env ruby
##
## toyunda2frm.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Sun Apr 25 17:56:34 2004 Olivier Leclant
## $Id: toyunda2frm.rb 1.2 Thu, 23 Sep 2004 17:49:00 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

# get the frame numbers back from a txt toyunda file

ToyundaChar = "�"

vieux_hack = [:vieux, :hack, :moisi]
while line = gets do
  next if not line.match(ToyundaChar)
  if (m = line.match(/^\{([0-9]*)\}\{([0-9]*)\}/)) and m[1..2] != vieux_hack[1..2] then
    print m[1], " ", m[2], "\n"
    vieux_hack = m
  end
end
