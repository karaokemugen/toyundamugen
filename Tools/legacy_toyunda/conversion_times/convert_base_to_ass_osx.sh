# Version spéciale pour OSX
# Nécessite homebrew
# il faut installer ffmpeg
# brew install ffmpeg
# brew install gnu-sed
# Oui il faut le gnu sed car le sed de OSX est caca.

CHEMIN="../../../kara"
INIFILES=`ls $CHEMIN/ini/`
rm erreurs.txt
while read INIFILE
do
	grep .txt "$CHEMIN/ini/$INIFILE" 2>/dev/null >/dev/null
	if [ $? -eq 0 ]
	then
		LYRICS=`grep -ai file "$CHEMIN/ini/$INIFILE" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
		KARA=`basename "$LYRICS" .txt`
		VIDEO=`grep -ai aviname "$CHEMIN/ini/$INIFILE" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
		#Detection si c'est un timing v3		
		grep "LYRICS - GENERATE AGAIN AFTER YOU EDIT" "$CHEMIN/Lyrics/$LYRICS" 2>/dev/null >/dev/null
		if [ $? -eq 0 ]
		then
			#C'est un timing v3, il va falloir extraire. D'abord le .lyr
			# Récupérer lignes entre :
			# # --- LYRICS - GENERATE AGAIN AFTER YOU EDIT ---
			# et
			# # --- SUB - DO NOT EDIT HERE - MODIFICATIONS WILL BE LOST ---
			# virer les -- en début de ligne
			# enregistrer en .lyr

			gsed -n '/# --- LYRICS - GENERATE AGAIN AFTER YOU EDIT ---/,/# --- SUB - DO NOT EDIT HERE - MODIFICATIONS WILL BE LOST ---/p' "$CHEMIN/Lyrics/$LYRICS" |  gsed 's/^--//g' >"kara/$KARA.lyr"
			# Si sed échoue ça veut dire qu'il y a des caractères parasites dans le kara.
			if [ $? -eq 0 ]
			then
		
				#Ensuite le .frm
				# # --- TIMING - GENERATE AGAIN AFTER YOU EDIT ---
				# et 
				# fin du fichier
				# supprimer les ==
				# enregistrer en .frm
	
				gsed -n '/# --- TIMING - GENERATE AGAIN AFTER YOU EDIT ---/,$p' "$CHEMIN/Lyrics/$LYRICS" | gsed 's/^==//g' >"kara/$KARA.frm"

	
				# On récupère le framerate
			
				ffmpeg -i "$CHEMIN/Videos/$VIDEO" 2>fps.txt
				FPS=`cat fps.txt | grep fps | awk -F, '{for(i=1;i<=NF;i++){if ($i ~ /fps/){print $i}}}' | awk -F\  {'print $1'}`
				rm fps.txt
				echo "ruby toyundagen2ass.rb \"kara/$KARA.lyr\" \"kara/$KARA.frm\" $FPS"
				ruby toyundagen2ass.rb "kara/$KARA.lyr" "kara/$KARA.frm" $FPS >"ass/$KARA.ass"			
				RC=$?
				if [ $RC -ne 0 ]
					then
					echo "$KARA | Erreur de conversion" >>erreurs.txt
				fi
				
			else
				#Kara invalide
				echo "$KARA contient des caractères invalides!"
				echo "$KARA | Caracteres invalides" >>erreurs.txt
			fi
		else 
			#Ce n'est pas un timing v3, on le note
			echo "$KARA n'est pas un timing v3!"
			echo "$KARA | Pas un timing v3!" >>erreurs.txt
		fi
		
	fi
	echo "====================================="
done <<< "$INIFILES"

