What is EpitASS ?
==============

EpitASS is a converter for karaoke subtitle files from Toyunda Raw format to V4+ aegisub-generated-style ASS format.
Project repository : https://framagit.org/Seipas/EpitASS

How to build it ?
==============

- Install `ocamlrun`, `ocamlc`, `ocamllex` and `ocamlyacc`
- Run `make`


If you want to built it by hand, here is what `make build` does :

```sh
ocamlc -i tree_toyunda_raw.ml > tree_toyunda_raw.mli
ocamlc -c tree_toyunda_raw.mli
ocamlc -c tree_toyunda_raw.ml
ocamlyacc -v parser_toyunda_raw.mly
ocamlc -c parser_toyunda_raw.mli
ocamlc -c parser_toyunda_raw.ml
ocamllex lexer_toyunda_raw.mll
ocamlc -i lexer_toyunda_raw.ml > lexer_toyunda_raw.mli
ocamlc -c lexer_toyunda_raw.mli
ocamlc -c lexer_toyunda_raw.ml
ocamlc -i tree_v4p_ass.ml > tree_v4p_ass.mli
ocamlc -c tree_v4p_ass.mli
ocamlc -c tree_v4p_ass.ml
ocamlc -i toyunda_raw_to_v4p_ass.ml > toyunda_raw_to_v4p_ass.mli
ocamlc -c toyunda_raw_to_v4p_ass.mli
ocamlc -c toyunda_raw_to_v4p_ass.ml
ocamlc -i epitass.ml > epitass.mli
ocamlc -c epitass.mli
ocamlc -c epitass.ml
ocamlc -o epitass tree_toyunda_raw.cmo parser_toyunda_raw.cmo lexer_toyunda_raw.cmo tree_v4p_ass.cmo toyunda_raw_to_v4p_ass.cmo epitass.cmo
```

How to use it ?
==============

Usage :
```
./epitass [options] toyunda_file
```

The ass file is printed by default on standard output, the options should always be before the file and are processed left to right (in case of collision, the last one have priority).
To see a list of options and the default behaviours, type :
```
./epitass -help
```

For v1 files, you may want to add the option -overlap_frame_mode

Example :
```
./epitass -video Nichijou-OP1-Kakakaka-Kataomoi-C.avi -framerate 25 -o Nichijou-OP1-Kakakaka-Kataomoi-C.ass Nichijou-OP1-Kakakaka-Kataomoi-C.txt
```



What are those formats ?
==============

SSA/ASS and Toyunda are formats used to give subtitles to a video. These subtitles can be karaoke subtitles, showing which syllable is voiced when.


SSA/ASS
--------------
SSA stands for Sub Station Alpha. It's a format used by the programs SubStation Alpha and Aegisub. It has several versions : V1, V2, V3, V4 and V4+, the later is called Advanced SubStation Alpha (ASS).

Modern Players can usually read those files without modifications.

EpitASS use the V4+, same format as Aegisub. For more informations :
http://fileformats.wikia.com/wiki/SubStation_Alpha
http://docs.aegisub.org/3.2/Main_Page/


Toyunda
--------------
Toyunda is a format used by members of Epitanime, the anime club of French private engineering school Epita, hence the name of this program.

In order to read those files, you have to install a plugin to your player.

In addition to not being compatible, it may not be very flexible, lack functionnalities, and have technical problems, so you may want to convert your files in ASS format.


Documentation can be found here : https://sites.google.com/site/stoyunda/documentation/format-raw

A Toyunda file contains several subtitles one for each line in the file, each subtitle having a start frame, end frame, options, and several lines, each line having a number of "pipes" (representing line breaks), options, and text. Usually, each subtitle have only one line. Options can be colors, size or positioning. On top of that, there is special lines containing -usually- only spaces and one 'ÿ' character. We'll call those lines "ylines", they represent when a syllable is played during the karaoke.


Usually, one does not write a raw toyunda file by hand : they write a .lyr and a .frm, and use a compiler to convert them into a raw toyunda file.

If you have those two files, you can convert them into a ASS file using a Ruby script called toyundagen2ass.rb

This program is intended to focus on the case where you don't have those files.


There are several versions of toyunda files :
- v1 don't have options, they are really simple : one subtitle/line by line of text shown in the end result.
- v2 have options, and each shown line in the end is in fact splitted between different subtitles/lines inside the file : one for fade in, three for each syllable (before-syllable-text in one color, syllable-text in another color, after-syllable-text in a third color), and one for fade-out.
- v3 and mikomi-generated : those files include .lyr and .frm files in comments. That means you can use toyundagen2ass.rb. EpitASS can be used on those files, but the result is not garanteed.



Assumptions and possibly unwanted behaviour
==============

- We will call "yline" a line containing a 'ÿ', only if it contains several ' ', one 'ÿ' and no other character, if it has an strictly positive number of pipes, and if it has no position (o:) option. In this case, we consider it as a special line indicating syllable partition, going with normal lines ("nline") with one pipe less. If it's not the case, it will be considered like a normal line and the ÿ will be printed. Also, size and color options are ignored for ÿ lines (with a warning).
- ylines come in the same order for the syllable place and the start/end frames : Katsudon can't be timed ka-don-tsu. If that happens, the line will be timed Katsu-don and the yline for tsu will be ignored with a warning, or matched with another buffer in the cluster.
- ylines don't overlap in time. If that happens, an ERROR line will be printed, and the generated file will have a negative \k tag.
- Positioned lines are not important lines : they are printed as is. If effects are done with this option, it will most likely turn into a disaster. That shouldn't happen in toyunda v1 and v2 but may occur in v3.
- Bitmap and DDR options are not important, they are ignored (with a warning).
- There is no line that covers a long duration, if that happens some lines with the same text but at two different time (for example a refrain) may be merged together.

If you want to understand better how epitass works (with the clusters and buffers), see the comments in toyunda_raw_to_v4p_ass.ml


Known bugs and lacking functionnalities
==============
- Positionned lines are not printed.
- Lack of command line options.
- Generated ass lines appear in the order of the pipes, not time.
- Style guess doesn't seem to work.
