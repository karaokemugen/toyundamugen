(******************************\
 *          Epitass            *
 *  toyunda_raw_to_v4p_ass.ml  *
 *                             *
 * here the black magic happen *
 \******************************)

(** Assumptions and possibly unwanted behaviour **)(*
  1) We will call "yline" a line containing a 'ÿ', only if it contains several ' ', one 'ÿ' and no other character, if it has an strictly positive number of pipes, and if it has no position (o:) option. In this case, we consider it as a special line indicating syllable partition, going with normal lines ("nline") with one pipe less. If it's not the case, it will be considered like a normal line and the ÿ will be printed. Also, size and color options are ignored for ÿ lines (with a warning).
  2) ylines come in the same order for the syllable place and the start/end frames : Katsudon can't be timed ka-don-tsu. If that happens, the line will be timed Katsu-don and the yline for tsu will be ignored with a warning, or matched with another buffer in the cluster.
  3) ylines don't overlap. If that happens, an ERROR line will be printed, and the generated file will have a negative \k tag.
  4) Positioned lines are not important lines : they are printed as if. If effects are done with this option, it will most likely turn into a disaster. That shouldn't happen in toyunda v1 and v2 but may occur in v3.
  5) Bitmap and DDR options are not important, they are ignored (with a warning).
  6) There is no line that covers a long duration, if that happens some lines with the same text but at two different time (for example a refrain) may be merged together.
*)

(** First step : sort toyunda lines **)(*
  1) Explode subtitles containing several lines in order to keep only lines (same for the options).
  2) Sort each line by the number of pipe they contains. Consider the lines containing only spaces and ÿ like a special line and keep it with the lines containing one pipe less. Keep the lines with a positioning (o:) option together.
  3) Make clusters of line by the interval of time they covers (example with 5 lines : [1,3], [2,5], [5,6], [10,11], [9,12] will end up in 2 clusters : [1,6], [9,12]).
*)
module IntOrd =
struct
   type t = int
   let compare = compare
end
module Imap = Map.Make(IntOrd)

type line =
  | Nline of nline
  | Yline of yline
and nline = int (*begin_frame*) * int (*end_frame*) * int (*pipe_number*) * color_option * size_option * position_option * string
and yline = int (*begin_frame*) * int (*end_frame*) * int (*pipe_number (one less that the actual number)*) * int (*ycursor*)
and color_option =
  | DefaultColor
  | Color of color (*a,b,g,r*)
  | ProgressiveColor of color*color
and color = int*int*int*int
and size_option =
  | DefaultSize
  | Size of int
  | ProgressiveSize of int*int
and position_option =
  | DefaultPosition
  | Position of int*int
  | ProgressivePosition of (int*int)*(int*int)

and sorted_toyunda = cluster list Imap.t (*int keys : number of pipes. ylines with n+1 pipes belong to the n group *)

and cluster = int (*begin_frame*) * int (*end_frame*) * nline list * yline list

let rec insert_line_in_cluster_list overlap_frame_mode startf endf l cl =
  let create_cluster s e l = match l with
    | Yline yl -> (s,e,[],[yl])
    | Nline nl -> (s,e,[nl],[])
  in
  let rec merge_cluster (s,e,nll,yll) cl = match cl with
    | [] ->  [(s,e,nll,yll)]
    | (s',e',nll',yll')::clt ->
      if e<s' || (overlap_frame_mode && e=s')
      then (s,e,nll,yll)::cl
      else merge_cluster ((if s<s' then s else s'), (if e>e' then e else e'), List.merge (Pervasives.compare) nll nll', List.merge (Pervasives.compare) yll yll') clt
  in
  match cl with
    | [] -> [create_cluster startf endf l]
    | (s,e,nll,yll)::clt ->
      if startf > e || (overlap_frame_mode && startf=e)
      then (s,e,nll,yll)::(insert_line_in_cluster_list overlap_frame_mode startf endf l clt)
      else merge_cluster (create_cluster startf endf l) cl


let sort_toyunda overlap_frame_mode k =
  let ((sorted_toyunda, positioned_lines), _) =
    List.fold_left
    (fun ((st,positioned_lines),previous_endf) (start, endf, subopts, lines) ->

      let startf = match start with
        | Tree_toyunda_raw.Begin_follow_previous -> previous_endf + 1
        | Tree_toyunda_raw.Begin_frame f -> f
      in

      let (co, so, po) = List.fold_left (fun (co,so,po) o -> match o with
        | Tree_toyunda_raw.Subtitle_color_fixed (a,b,g,r) -> (Color (255-a,b,g,r), so, po)
        | Tree_toyunda_raw.Subtitle_color_progressive ((a1,b1,g1,r1),(a2,b2,g2,r2)) -> (ProgressiveColor ((255-a1,b1,g1,r1),(255-a2,b2,g2,r2)), so, po) 
        | Tree_toyunda_raw.Subtitle_size_fixed s -> (co, Size s, po)
        | Tree_toyunda_raw.Subtitle_size_progressive (s1,s2) -> (co,ProgressiveSize (s1,s2),po)
        | Tree_toyunda_raw.Subtitle_position_fixed (x,y) -> (co,so,Position (x,y))
        | Tree_toyunda_raw.Subtitle_position_progressive (p1,sp2) -> (co,so,ProgressivePosition (p1,sp2))
        | Tree_toyunda_raw.Subtitle_bitmap b -> let _ = Printf.eprintf "[Warning] Bitmap option are not handled by this program. \"%s\" encountered in a subtitle bitmap option." b in (co,so,po)
        | Tree_toyunda_raw.Subtitle_ddr d -> let _ = Printf.eprintf "[Warning] DDR option are not handled by this program. \"%s\" encountered in a subtitle ddr option." d in (co,so,po)
      ) (DefaultColor, DefaultSize, DefaultPosition) subopts
      in

      ((
        List.fold_left
        (fun (st,positioned_lines) (pipe_num, lineopts, text) ->

          let (co, so, po) = List.fold_left (fun (co,so,po) o -> match o with
            | Tree_toyunda_raw.Line_color_fixed (a,b,g,r) -> (Color (255-a,b,g,r), so, po)
            | Tree_toyunda_raw.Line_color_progressive ((a1,b1,g1,r1),(a2,b2,g2,r2)) -> (ProgressiveColor ((255-a1,b1,g1,r1),(255-a2,b2,g2,r2)), so, po) 
            | Tree_toyunda_raw.Line_size_fixed s -> (co, Size s, po)
            | Tree_toyunda_raw.Line_size_progressive (s1,s2) -> (co,ProgressiveSize (s1,s2),po)
            | Tree_toyunda_raw.Line_position_fixed (x,y) -> (co,so,Position (x,y))
            | Tree_toyunda_raw.Line_position_progressive (p1,sp2) -> (co,so,ProgressivePosition (p1,sp2))
            | Tree_toyunda_raw.Line_bitmap b -> let _ = Printf.eprintf "[Warning] Bitmap option are not handled by this program. \"%s\" encountered in a line bitmap option." b in (co,so,po)
            | Tree_toyunda_raw.Line_ddr d -> let _ = Printf.eprintf "[Warning] DDR option are not handled by this program. \"%s\" encountered in a line ddr option." d in (co,so,po)
          ) (co,so,po) lineopts in

          let ycursor t =
            let rec aux i b =
              try
                if t.[i] = ' '
                then aux (i+1) b
                (*else if b = -1 && (t.[i] = '\195') && (t.[i+1] = '\191')
                then aux (i+2) i*)
                else if b = -1 && (t.[i] = '\255')
                then aux (i+1) i
                else -1
              with Invalid_argument _ -> b
            in aux 0 (-1)
          in

          let yc = (ycursor text) in
          (*let _ = Printf.eprintf "[test] yc=%d for line '%s'\n" yc text in let _ = prerr_endline "" in*)
          if(pipe_num>0 && (po==DefaultPosition) && yc>=0)
          then
            let pipe_num = pipe_num - 1 in
            let yline = (startf, endf, pipe_num, yc) in
            let cl =
              try
                Imap.find pipe_num st
              with Not_found -> []
            in
            let ncl = insert_line_in_cluster_list overlap_frame_mode startf endf (Yline yline) cl in
            (Imap.add pipe_num ncl st,positioned_lines)
          else
            let _ = if yc>=0 then Printf.eprintf "[Warning] A ÿ line starting at frame %d was not considered as one since %s.\n" startf (if(pipe_num=0) then "it has no pipe" else "it is positionned") else () in
            let nline = (startf, endf, pipe_num, co, so, po, text) in
            if po==DefaultPosition
            then
              let cl =
                try
                  Imap.find pipe_num st
                with Not_found -> []
              in
              let ncl = insert_line_in_cluster_list overlap_frame_mode startf endf (Nline nline) cl in
              (Imap.add pipe_num ncl st,positioned_lines)
            else
              (st,nline::positioned_lines)
        ) (st,positioned_lines) lines
      ),endf)
    ) ((Imap.empty,[]),-1) k in
  (sorted_toyunda, positioned_lines)



let fprint_sorted_toyunda outch sorted_toyunda =

  let pc co = match co with
    | DefaultColor -> ""
    | Color (a,b,g,r) -> String.concat "" ["{c:";string_of_int a;",";string_of_int b;",";string_of_int g;",";string_of_int r;"}"]
    | ProgressiveColor ((a,b,g,r),(a',b',g',r')) -> String.concat "" ["{c:";string_of_int a;",";string_of_int b;",";string_of_int g;",";string_of_int r;":";string_of_int a';",";string_of_int b';",";string_of_int g';",";string_of_int r';"}"]
  in
  let ps so = match so with
    | DefaultSize -> ""
    | Size s -> String.concat "" ["{s:";string_of_int s;"}"]
    | ProgressiveSize (s,s') -> String.concat "" ["{s:";string_of_int s;":";string_of_int s';"}"]
  in
  let pp po = match po with
    | DefaultPosition -> ""
    | Position (x,y) -> String.concat "" ["{p:";string_of_int x;",";string_of_int y;"}"]
    | ProgressivePosition ((x,y),(x',y')) -> String.concat "" ["{p:";string_of_int x;",";string_of_int y;":";string_of_int x';",";string_of_int y';"}"]
  in
  let ppn pn = String.make pn '|' in

  Imap.fold (fun pn cl () ->
    let _ = Printf.fprintf outch "Clusters with %d pipes\n" pn in
    List.fold_left (fun () (bf, ef, nll, yll) ->
      let _ = Printf.fprintf outch "  Cluster from %d to %d\n" bf ef in
      let _ = List.fold_left (fun () (bf, ef, pn, co,so,po, text) ->
        Printf.fprintf outch "    Line {%d}{%d}%s%s%s%s%s\n" bf ef (ppn pn) (pc co) (ps so) (pp po) text
      ) () nll in
      List.fold_left (fun () (bf, ef, pn, yc) ->
        Printf.fprintf outch "    YLine {%d}{%d}%sycursor=%d\n" bf ef (ppn pn) yc
      ) () yll
    ) () cl
  ) sorted_toyunda ()



(*Convert latin1 string to utf8. Toyunda files use latin1, the program prints utf-8. Will be usefull in second step.*)
let utf8_of_latin1 s =
  let rec uol i ns =
    try
      let c = s.[i] in
      if c < '\160'
      then uol (i+1) (Printf.sprintf "%s%c" ns c)
      else
        let nc =
               if c='\160' then " "
          else if c='\161' then "¡"
          else if c='\162' then "¢"
          else if c='\163' then "£"
          else if c='\164' then "¤"
          else if c='\165' then "¥"
          else if c='\166' then "¦"
          else if c='\167' then "§"
          else if c='\168' then "¨"
          else if c='\169' then "©"
          else if c='\170' then "ª"
          else if c='\171' then "« "
          else if c='\172' then "¬"
          else if c='\173' then "-"
          else if c='\174' then "®"
          else if c='\175' then "¯"
          else if c='\176' then "°"
          else if c='\177' then "±"
          else if c='\178' then "²"
          else if c='\179' then "³"
          else if c='\180' then "´"
          else if c='\181' then "µ"
          else if c='\182' then "¶"
          else if c='\183' then "·"
          else if c='\184' then "¸"
          else if c='\185' then "¹"
          else if c='\186' then "º"
          else if c='\187' then " »"
          else if c='\188' then "¼"
          else if c='\189' then "½"
          else if c='\190' then "¾"
          else if c='\191' then "¿"
          else if c='\192' then "À"
          else if c='\193' then "Á"
          else if c='\194' then "Â"
          else if c='\195' then "Ã"
          else if c='\196' then "Ä"
          else if c='\197' then "Å"
          else if c='\198' then "Æ"
          else if c='\199' then "Ç"
          else if c='\200' then "È"
          else if c='\201' then "É"
          else if c='\202' then "Ê"
          else if c='\203' then "Ë"
          else if c='\204' then "Ì"
          else if c='\205' then "Í"
          else if c='\206' then "Î"
          else if c='\207' then "Ï"
          else if c='\208' then "Ð"
          else if c='\209' then "Ñ"
          else if c='\210' then "Ò"
          else if c='\211' then "Ó"
          else if c='\212' then "Ô"
          else if c='\213' then "Õ"
          else if c='\214' then "Ö"
          else if c='\215' then "×"
          else if c='\216' then "Ø"
          else if c='\217' then "Ù"
          else if c='\218' then "Ú"
          else if c='\219' then "Û"
          else if c='\220' then "Ü"
          else if c='\221' then "Ý"
          else if c='\222' then "Þ"
          else if c='\223' then "ß"
          else if c='\224' then "à"
          else if c='\225' then "á"
          else if c='\226' then "â"
          else if c='\227' then "ã"
          else if c='\228' then "ä"
          else if c='\229' then "å"
          else if c='\230' then "æ"
          else if c='\231' then "ç"
          else if c='\232' then "è"
          else if c='\233' then "é"
          else if c='\234' then "ê"
          else if c='\235' then "ë"
          else if c='\236' then "ì"
          else if c='\237' then "í"
          else if c='\238' then "î"
          else if c='\239' then "ï"
          else if c='\240' then "ð"
          else if c='\241' then "ñ"
          else if c='\242' then "ò"
          else if c='\243' then "ó"
          else if c='\244' then "ô"
          else if c='\245' then "õ"
          else if c='\246' then "ö"
          else if c='\247' then "÷"
          else if c='\248' then "ø"
          else if c='\249' then "ù"
          else if c='\250' then "ú"
          else if c='\251' then "û"
          else if c='\252' then "ü"
          else if c='\253' then "ý"
          else if c='\254' then "þ"
          else if c='\255' then "ÿ"
          else
            let s= String.make 1 c in
            let _ = Printf.eprintf "[Warning] unrecognised character : \'%s\'\n" (String.escaped s) in
            s
        in uol (i+1) (String.concat "" [ns;nc])
    with Invalid_argument _ -> ns
  in
  uol 0 ""



(** Second step : black magic in clusters **)(*
  1) Manage each cluster separately
  2) For each line in the cluster, check the letters in the text, and try matching them with buffers ignoring space caracters (example : the line "   ba    " matches with the buffer "Barbapapa"), if it doesn't match, create a new one. Keep the link between the line and the matched buffer.
  3) If there is one buffer, associate all ylines of the cluster with it. Is several buffers (it souldn't be the case), print a warning, then try splitting the ylines between them with the starting/ending time.
  4) Split the text of the buffer with {\k} ass tags using ylines
  5) Try to create a style for each cluster or use one that already exist
*)

type style = int(*id*) * int(*font_size*) * color(*primaryColour*) * color(*secondaryColour*)

let create_ass_events_and_styles
font shadow_color bold italic underline strike_out scalex scaley spacing angle borderstyle outline shadow alignment marginl marginr
default_font_size default_primary_color default_secondary_color default_outline_color default_marginv default_alpha framerate
color_threshold override_style_mode override_alpha_mode backup_first_style_mode guess_outline_color_mode warning_following_syllable_mode
sorted_toyunda positioned_lines =

  (*For each pipe number*)
  Imap.fold (fun pipes cl (style_list, event_list) ->
  (*let _ = prerr_endline "[Info]  Pipe number\n" in*)

    (*For each cluster*)
    let (style_list, event_list) =
      List.fold_left (fun (style_list, event_list) (sf,ef,nll,yll) ->
      (*let _ = prerr_endline "[Info]   Cluster\n" in*)

        (*let _ = prerr_endline "[Info]    buffer_list\n" in*)
        (*For each normal line in the cluster, try matching it with a buffer or create a new one *)
        let buffer_list =
          List.fold_left (fun bl (sfl, efl, pn, co, so, po, text) ->

            (*let rec de_facto_length t i =
              try
                if t.[i] = ' '
                then de_facto_length t (i+1)
                else 1 + (de_facto_length t (i+1))
              with Invalid_argument _ -> 0
            in*)
            let rec last_not_space_character s last i =
              try
                if s.[i] = ' '
                then last_not_space_character s last (i+1)
                else last_not_space_character s i (i+1)
              with Invalid_argument _ -> last
            in
            let ltxt = (last_not_space_character text (-1) 0) + 1 in
            (*let _ = Printf.eprintf "[test] %stxt='%s' / ltxt=%d / String.length text=%d" (if ltxt>(String.length text) then "ERROR" else "") text ltxt (String.length text) in let _ = prerr_endline "" in*)

            let rec match_text_buffer b lb text ltxt i =
            (*let _ = if i=0 || i=lb then Printf.eprintf "[test] text='%s' / ltxt=%d / b='%s' / lb=%d / i=%d\n" text ltxt b lb i else () in*)
            (*let _ = if ltxt>(String.length text) then Printf.eprintf "[ERROR] txt='%s' / ltxt=%d / String.length text=%d\n" text ltxt (String.length text) else () in
            let _ = if lb!=(String.length b) then Printf.eprintf "[ERROR] b='%s' / lb=%d / String.length b=%d\n" b lb (String.length b) else () in*)
              if i>=ltxt
                then (true, b, lb)
              else if i>=lb
                then
                  let rest = (String.sub text i (ltxt-i)) in
                  (true, (String.concat "" [b;rest]), ltxt)
              else if text.[i] = ' '
                then match_text_buffer b lb text ltxt (i+1)
              else if b.[i] = ' '
                then match_text_buffer (String.concat "" [
                  (try String.sub b 0 i with Invalid_argument _ -> let _ = Printf.eprintf "[ERROR] plif\n" in "");
                  (String.make 1 (text.[i]));
                  (try String.sub b (i+1) (lb-i-1) with Invalid_argument _ -> let _ = Printf.eprintf "[ERROR] plouf\n" in "")
                ]) lb text ltxt (i+1)
              else if text.[i] = b.[i]
                then match_text_buffer b lb text ltxt (i+1)
              else (false, b, lb)
            in

            let rec match_text bl text ltxt l =
              match bl with
                | [] -> raise Not_found (*We don't want to create the new buffer at the end of the list, because we want to check the latest buffer created first*)
                | (sfb,efb,pn,b,lb,ll)::blt ->
                  let (matchf, newb, newbl) =  match_text_buffer b lb text ltxt 0 in
                  (*let _ = Printf.eprintf "[test] %s b=\"%s\" lb=%d" (if matchf then "matchf" else "dropped") newb newbl in let _ = prerr_endline "" in*)
                  if matchf
                  then
                    (min sfl sfb, max efl efb, pn, newb, newbl, (ltxt, l)::ll)::blt
                  else
                    (sfb,efb,pn,b,lb,ll)::(match_text blt text ltxt l)
            in

            try
              match_text bl text ltxt (sfl, efl, pn, co, so, po, text)
            with Not_found ->
              (*let _ = Printf.eprintf "[test] buffer creation b=\"%s\" lb=%d" text ltxt in let _ = prerr_endline "" in*)
              (sfl, efl, pn, text, ltxt, [(ltxt,(sfl, efl, pn, co, so, po, text))])::bl
          ) [] nll
        in

        (*let _ = prerr_endline "[Info]    stylized_buffer_list\n" in*)
        (* Try finding style for each buffer *)
        let (style_list, stylized_buffer_list) = List.fold_left (fun (sl,sbl) (sfb,efb,pn,b,lb,ll) ->
          (* TODO : try guessing syllables from the lines and then check if it correspond to the ylines to print a warning *)
          if override_style_mode
          then
            (sl, ("Default",sfb, efb, pn, b, lb,[])::sbl)
          else
            let rec guess_style_list (co,so) ll = match ll with
              | [] -> (co,so)
              | (ltxt, (sfl, efl, pipes, color_option, size_option, position_option, text))::llt ->
                let diff_color (_,b1,g1,r1) (_,b2,g2,r2) =
                  (abs (b1-b2)) + (abs (g1-g2)) + (abs (r1-r2))
                in
                let (nco, coset) =
                (match co with
                  | DefaultColor ->
                    (match color_option with
                      | Color c -> (Color c,false)
                      | _ -> (co,false)
                    )
                  | Color c ->
                    (match color_option with
                      | Color c' -> if (diff_color c' c) < color_threshold then (Color c, false) else (ProgressiveColor (c,c'),true)
                      | _ -> (co,false)
                    )
                  | ProgressiveColor _ -> (co,true)
                ) in
                let (nso, soset) =
                (match so with
                  | DefaultSize ->
                    (match size_option with
                      | DefaultSize -> (Size 30,true)
                      | Size s -> (Size s,true)
                      | _ -> (so,false)
                    )
                  | Size s -> (so,true)
                  | _ -> (so,true)
                ) in
                if coset && soset then(nco, nso)
                else guess_style_list (nco,nso) llt
            in

            (*let _ = prerr_endline "[Info]     guess_style_list\n" in*)
            let (co,so) = guess_style_list (DefaultColor,DefaultSize) ll in
            let size = match so with
              | DefaultSize -> default_font_size
              | Size s -> (default_font_size * s / 30)
              | _ -> default_font_size
            in
            let (primary_color,secondary_color,back_color) = match co with
              | ProgressiveColor ((a1,b1,g1,r1),(a2,b2,g2,r2)) ->
                (
                  ((if override_alpha_mode then default_alpha else a2),b2,g2,r2),
                  ((if override_alpha_mode then default_alpha else a1),b1,g1,r1),
                  (if guess_outline_color_mode then ((if override_alpha_mode then default_alpha else (a1+a2)/2),255 - ((b1+b2)/2),255 - ((g1+g2)/2),255 - ((r1+r2)/2)) else default_outline_color)
                )
              | _ -> (default_primary_color,default_secondary_color,default_outline_color)
            in

            (*let _ = prerr_endline "[Info]     add_style\n" in*)
            let rec add_style primary_color secondary_color outline_color size sl i = match sl with
              |[] ->
                let stylename = String.concat "" ["GeneratedStyle"; string_of_int i] in
                ([(
                  stylename,font,size,
                  primary_color, secondary_color,outline_color,shadow_color,
                  bold,italic,underline,strike_out,scalex,scaley,spacing,angle,
                  borderstyle,outline,shadow,alignment,marginl,marginr,default_marginv,1,[]
                )],stylename)
              |style::slt -> (match style with (stylename',_,size',c1,c2,_,c4,_,_,_,_, _,_,_,_,_,_,_,_,_,_,_,_,_) ->
                if (size' = size) && (primary_color = c1) && (secondary_color == c2) && (back_color = c4)
                then
                  (sl,stylename')
                else
                  let (rsl,stylename) = add_style primary_color secondary_color back_color size slt (i+1) in
                  (style::rsl,stylename))
            in

            let (style_list,stylename) = add_style primary_color secondary_color back_color size style_list 0 in
            (style_list, (stylename, sfb, efb, pn, b, lb,[])::sbl)

        ) (style_list,[]) buffer_list
        in

        (*let _ = prerr_endline "[Info]    sort\n" in*)
        let yll = List.sort (fun (bf1, ef1, _, yc1) (bf2, ef2, _, yc2) -> compare (bf1,yc1,ef1) (bf2,yc2,ef2)) yll in
        let stylized_buffer_list = List.sort (fun (_, sfb1, efb1, _, _, _, _) (_, sfb2, efb2, _, _, _, _) -> compare (sfb1, efb1) (sfb2, efb2)) stylized_buffer_list in

        (*let _ = prerr_endline "[Info]    timed_buffer_list\n" in*)
        let timed_buffer_list = List.fold_left (fun tbl (sf, ef, _, ycursor) ->
          let rec insert_syllabe (sf,ef,yc) syll =  match syll with
            | [] -> [(sf,ef,yc)]
            | (sf',ef',yc')::syllt ->
              if sf=sf'
              then raise Exit
              else if sf<sf'
                then if yc<yc'
                  then (sf,ef,yc)::syll
                  else raise Exit
                else if yc>yc'
                  then (sf',ef',yc')::(insert_syllabe (sf,ef,yc) syllt)
                  else raise Exit
          in
          let rec find_syllable_in_buffers tbl = match tbl with
            | [] -> let _ = Printf.eprintf "[Warning] yline from %d to %d did not match any buffer\n" sf ef in tbl
            | (stylename, sfb, efb, pn, b, lb, syllable_list)::tblt -> 
              if ef > efb
              then (stylename, sfb, efb, pn, b, lb, syllable_list)::(find_syllable_in_buffers tblt)
              else
                try (stylename, sfb, efb, pn, b, lb, (insert_syllabe (sf,ef,ycursor) syllable_list))::tblt
                with Exit -> (stylename, sfb, efb, pn, b, lb, syllable_list)::(find_syllable_in_buffers tblt)
          in
          find_syllable_in_buffers tbl
        ) stylized_buffer_list yll
        in

        let event_list = List.fold_left (fun el (stylename, sfb, efb, pn, b, lb, syllable_list) ->
          let cs_of_frame f =
            int_of_float (
              ((float_of_int (f * 100)) /. framerate) -. 0.5
            )
          in
          let rec time b syllable_list first_frame expected_frame already_timed cursor_count = match syllable_list with
            | [] -> (first_frame, expected_frame, (String.concat ""
              [
                already_timed;
                try utf8_of_latin1 (String.sub b cursor_count ((String.length b) - cursor_count))
                with Invalid_argument _ -> let _ = Printf.eprintf "[Debug] b=%s cc=%d\n" b cursor_count in ""
              ]))
            | (sf,ef,yc)::slt ->
              let (first_frame, invisible_tag) =
                if sf != expected_frame
                then if expected_frame == first_frame
                  then (sf,"")
                  else
                    let _ =
                      if sf < expected_frame
                        then Printf.eprintf "[ERROR] The previous syllable was too long (%d>%d), printing a negative \\k tag.\n" expected_frame sf
                      else if warning_following_syllable_mode
                        then Printf.eprintf "[Warning] Expected next syllable frame %d, got %d.\n" expected_frame sf
                      else () in
                      (first_frame, String.concat "" ["{\\k";string_of_int (cs_of_frame (sf-expected_frame));"}"])
                else (first_frame,"") in
              let kstring = string_of_int (cs_of_frame (ef-sf)) in
              (*let _ = Printf.eprintf "[Test] kstring %s for ef-sf = %d-%d = %d | expected_frame=%d first_frame=%d \n" kstring ef sf (ef-sf) expected_frame first_frame in*)
              time b slt first_frame ef (
                String.concat "" [
                  already_timed;
                  (try utf8_of_latin1 (String.sub b cursor_count (yc - cursor_count))
                  with Invalid_argument _ -> let _ = Printf.eprintf "[Debug] b=%s cc=%d yc=%d\n" b cursor_count yc in "");
                  invisible_tag; "{\\k";kstring;"}"
                ]
              ) yc
          in
          let (ff,ef,tb) = time b syllable_list 0 0 "" 0 in
          let ff = cs_of_frame ff in
          let ef = cs_of_frame ef in
          (
            0, Tree_v4p_ass.cs_to_event_time (max (ff-90) 0), Tree_v4p_ass.cs_to_event_time (ef+10),
            stylename,"", marginl, marginr,(default_marginv + pn*(default_font_size+default_marginv)/2),
            "karaoke", (String.concat "" ["{\\fad(";string_of_int (max 0 (min (ff-150) 850));",100)\\k";string_of_int (min ff 90);"}"; tb]),[]
          )::el
        ) event_list timed_buffer_list
        in
        (style_list, event_list)
      ) (style_list, event_list) cl
    in

    let style_list =
    if backup_first_style_mode && override_style_mode
    then match style_list with
      | default_style::((stylename,_,size,c1,c2,c3,_,_,_,_,_, _,_,_,_,_,_,_,_,_,_,_,_,_)::slt) ->
        (
          "Default",font,size,
          c1,c2,c3,shadow_color,
          bold,italic,underline,strike_out,scalex,scaley,spacing,angle,
          borderstyle,outline,shadow,alignment,marginl,marginr,default_marginv,1,[]
        )::slt
      |_ -> style_list
    else style_list
    in
    (style_list,event_list)
  ) sorted_toyunda
  ([(
    "Default",font,default_font_size,
    default_primary_color,default_secondary_color,default_outline_color,shadow_color,
    bold,italic,underline,strike_out,scalex,scaley,spacing,angle,
    borderstyle,outline,shadow,alignment,marginl,marginr,default_marginv,1,[]
  )], [])



let build_ass (style_list : Tree_v4p_ass.style_values list) (event_list : Tree_v4p_ass.event_values list) playResX playResY audio_file video_file =
(
  (playResX,playResY,audio_file,video_file,[],[]),
  ([],style_list),
  ([],List.fold_left (fun rel el -> (true,el)::rel) [] event_list),
  "",
  ""
)
