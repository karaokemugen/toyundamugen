(****************************************************\
 *                      Epitass                      *
 *                 tree_toyunda_raw.ml               *
 *                                                   *
 * structure of the trees storing toyunda raw syntax *
 \****************************************************)

  type t = subtitle list

  and subtitle = begin_frame * int (*end_frame*) * subtitle_option list * line list

  and begin_frame =
    | Begin_follow_previous
    | Begin_frame of int

  and subtitle_option =
    | Subtitle_color_fixed of color
    | Subtitle_color_progressive of color * color
    | Subtitle_size_fixed of int (* 30 is default *)
    | Subtitle_size_progressive of int * int
    | Subtitle_position_fixed of position
    | Subtitle_position_progressive of position * position
    | Subtitle_bitmap of string
    | Subtitle_ddr of string (* can be changed to arrow list *)

  and color = int * int * int * int (* a,b,g,r ∈ [0;255]⁴ *)
  and position = int * int (* x,y ∈ [(0,0) ; (800,600)]*)

  and line = int (*pipe_number*) * line_option list * string (*actual text of the line, without differentiating ÿ lines*)

  and line_option =
    | Line_color_fixed of color
    | Line_color_progressive of color * color
    | Line_size_fixed of int (* 30 is default *)
    | Line_size_progressive of int * int
    | Line_position_fixed of position
    | Line_position_progressive of position * position
    | Line_bitmap of string
    | Line_ddr of string (* can be changed to arrow list *)

  let print_begin_frame b = match b with
    | Begin_follow_previous -> print_string "{}"
    | Begin_frame b -> let _ = print_char '{' in let _ = print_int b in print_char '}'

  exception InvalidHexaNotation of string

  let hexa_digit_to_int h =
    let a = Char.code h in
    if a<=57 && a>=48
    then
      a - 48
    else if a>=65 && a<=70
    then
      a - 55
    else
      raise (InvalidHexaNotation (String.make 1 h))

  let hexa_to_int h =
    let l = String.length h in

      let rec aux i =
        if i= -1 then 0
        else hexa_digit_to_int h.[i] + 16 * aux (i-1)
      in

      aux (l-1)


  let parse_color h =
    let l = String.length h in
    if ((l != 7) && (l != 9)) || (h.[0] != '$') then raise (InvalidHexaNotation h) else
    if l=7 then
      (255, (hexa_digit_to_int h.[1])*16+(hexa_digit_to_int h.[2]), (hexa_digit_to_int h.[3])*16+(hexa_digit_to_int h.[4]), (hexa_digit_to_int h.[5])*16+(hexa_digit_to_int h.[6]))
    else
      ((hexa_digit_to_int h.[1])*16+(hexa_digit_to_int h.[2]), (hexa_digit_to_int h.[3])*16+(hexa_digit_to_int h.[4]), (hexa_digit_to_int h.[5])*16+(hexa_digit_to_int h.[6]), (hexa_digit_to_int h.[7])*16+(hexa_digit_to_int h.[8]))


  let print_int_hexa_2d i =
    let print_int_hexa_digit i =
      if i<10
      then print_char (Char.chr (i+48))
      else if i<16
      then print_char (Char.chr (i+55))
      else raise (InvalidHexaNotation (string_of_int i))
    in

    let _ = print_int_hexa_digit (i/16) in
    print_int_hexa_digit (i mod 16)


  let print_color (a,b,g,r) =
    let _ = print_char '$' in
    let _ = if(a!=255) then print_int_hexa_2d a else () in
    let _ = print_int_hexa_2d b in
    let _ = print_int_hexa_2d g in
    print_int_hexa_2d r

  let print_position (x,y) =
    let _ = print_int x in
    let _ = print_char ',' in
    print_int y

  let print_line_option l = match l with
    | Line_color_fixed c ->
      print_string "{c:" ; print_color c ; print_char '}'
    | Line_color_progressive (c1,c2) ->
      print_string "{c:" ; print_color c1 ; print_char ':' ; print_color c2 ;  print_char '}'
    | Line_size_fixed i ->
      print_string "{s:" ; print_int i ; print_char '}'
    | Line_size_progressive (i1,i2) ->
      print_string "{s:" ; print_int i1 ; print_char ':' ; print_int i2 ;  print_char '}'
    | Line_position_fixed p ->
      print_string "{o:" ; print_position p ; print_char '}'
    | Line_position_progressive (p1,p2) ->
      print_string "{o:" ; print_position p1 ; print_char ':' ; print_position p2 ;  print_char '}'
    | Line_bitmap f ->
      print_string "{b:" ; print_string f ; print_char '}'
    | Line_ddr d ->
      print_string "{d:" ; print_string d ; print_char '}'

  let rec print_line (pipes, line_opt, text) =
    let _ = print_string (String.make pipes '|') in
    let _ = List.fold_left (fun () lo -> print_line_option lo) () line_opt in
    print_string text

  let print_subtitle_option s = match s with
    | Subtitle_color_fixed c ->
      print_string "{C:" ; print_color c ; print_char '}'
    | Subtitle_color_progressive (c1,c2) ->
      print_string "{C:" ; print_color c1 ; print_char ':' ; print_color c2 ;  print_char '}'
    | Subtitle_size_fixed i ->
      print_string "{S:" ; print_int i ; print_char '}'
    | Subtitle_size_progressive (i1,i2) ->
      print_string "{S:" ; print_int i1 ; print_char ':' ; print_int i2 ;  print_char '}'
    | Subtitle_position_fixed p ->
      print_string "{O:" ; print_position p ; print_char '}'
    | Subtitle_position_progressive (p1,p2) ->
      print_string "{O:" ; print_position p1 ; print_char ':' ; print_position p2 ;  print_char '}'
    | Subtitle_bitmap f ->
      print_string "{B:" ; print_string f ; print_char '}'
    | Subtitle_ddr d ->
      print_string "{D:" ; print_string d ; print_char '}'

  let print_subtitle (b,e,sub_opt,lines) =
      let _ = print_begin_frame b in
      let _ = print_char '{' in
      let _ = print_int e in
      let _ = print_char '}' in
      let _ = List.fold_left (fun () so -> print_subtitle_option so) () sub_opt in
      List.fold_left (fun () l -> print_line l) () lines

  let print_kara k = List.fold_left (fun () s -> let _ = print_subtitle s in print_newline ()) () k
