(defconst toyunda-zoomfactor 0.64)
(defconst toyunda-line-height-mul 1.25)
(defconst toyunda-image-zoom (* toyunda-zoomfactor 30.85))
(defconst toyunda-affine-base 0) ; (* 11.0 2.3))
(defconst toyunda-screen-width 800)
(defconst toyunda-screen-height 600)
(defvar toyunda-face-hash (makehash 'equal))

(defun toyunda-BGR-to-RGB (string)
  "This function takes a string representing a color in BGR and returns it in #RGB."
  (concat "#" (substring string 4) (substring string 2 4) (substring string 0 2))
)

(defun toyunda-choose-color-face (num)
       "This function reads text and colorizes it accordingly."
       (progn
	 (let* ((begin (match-beginning num))
		(end (match-end num))
		(start (if (> (- end 6) begin)
			   (- end 6) begin))
		(text (buffer-substring start end))
		(facename (concat "toyunda-color-face-" text)))
	   (if (not (gethash text toyunda-face-hash))
	       (progn
		 (eval `(defface ,(intern facename) '((t :inherit font-lock-string-face)) facename))
		 (set-face-foreground (intern facename) (toyunda-BGR-to-RGB text))
		 (set-text-properties begin end `(face ,(intern facename)))
		 (puthash text t toyunda-face-hash)
               )
           )
	   facename
	 )
       )
)

(defun toyunda-char-width (subsize)
  "This function returns the width of a letter of size subsize."
  (/ (* toyunda-image-zoom subsize) 30)
)

(defun toyunda-line-width (string subsize)
  "This function returns the width of a string."
  (* (toyunda-char-width subsize) (length string))
)

(defun toyunda-line-height (subsize)
  "This function returns the line height."
  (* toyunda-line-height-mul subsize)
)

(defun toyunda-line-position (string subsize row)
  "This function returns the position of the line."
  (list (round (- (- (/ toyunda-screen-width 2) (/ (toyunda-line-width string subsize) 2)) toyunda-affine-base)) (round (* (toyunda-line-height subsize) row)))
)

(defun toyunda-get-size ()
  "This function returns the default size of the text."
  (save-excursion
    (beginning-of-line)
    (if (looking-at ".*{s:\\([0-9]+\\)}")
	(progn
	  (re-search-forward ".*{s:\\([0-9]+\\)}")
	  (string-to-int (match-string 1))
	  )
      (if (looking-at ".*{s:\\([0-9]+\\):[0-9]+}")
	  (progn
	    (re-search-forward ".*{s:\\([0-9]+\\):[0-9]+}")
	    (string-to-int (match-string 1))
	    )
	  30
	)
      )
  )
)

(defun toyunda-default-size ()
  "This function returns the default size of the text."
  (if (or (looking-at ".*{s:\\([0-9]+\\)}")
	  (looking-at ".*{s:\\([0-9]+\\):[0-9]+}"))
      ""
    "{s:30}")
)

(defun toyunda-default-color ()
  "This function returns the default color."
  (if (or (looking-at ".*\\({c:.[0-9A-Fa-f]+}\\)")
	  (looking-at ".*\\({c:.[0-9A-Fa-f]+:.[0-9A-Fa-f]+}\\)"))
      ""
    "{c:$FFFFFFFF}")
)

(defun toyunda-count-pipes (string)
  "This function counts the pipes in a string."
  (if (eq 0 (length string))
      0
    (if (char-equal ?\| (aref string 0))
	(+ 1 (toyunda-count-pipes (substring string 1)))
      (toyunda-count-pipes (substring string 1))
    )
  )
)

(defun toyunda-add-size (start end)
  "This function adds the default size of a line."
  (interactive "r")
  (toyunda-add-default start end 'toyunda-default-size)
)

(defun toyunda-add-color (start end)
  "This function adds the default size of a line."
  (interactive "r")
  (toyunda-add-default start end 'toyunda-default-color)
)

(defun toyunda-add-position (start end)
  "This function adds the default size of a line."
  (interactive "r")
  (toyunda-add-default start end 'toyunda-default-position)
  (replace-regexp "\|" "" nil start end)
)

(defun toyunda-add-default (start end function)
  "This functions adds the result of the function to the lines of a region."
  (save-excursion
    (goto-char start)
    (beginning-of-line)
    (while (re-search-forward "^{[0-9]+}{[0-9]*}" end t)
      (progn
	(goto-char (match-end 0))
	(insert (funcall function))
      )
    )
  )
)

(defun toyunda-default-position ()
  "This function returns the default position of the text."
  (save-excursion
    (beginning-of-line)
    (if (or (looking-at ".*\\({o:[0-9-]+,[0-9-]+}\\)")
	    (looking-at ".*\\({o:[0-9-]+,[0-9-]+:[0-9-]+,[0-9-]+}\\)"))
	""
      (if (re-search-forward "\\(^.*\\)}\\(|*\\)\\([^|}\n]+\\)")
	  (let ((text1 (match-string 3))
		(text2 (concat (match-string 1) (match-string 2))))
	    (let ((frames (toyunda-line-position text1 (toyunda-get-size) (toyunda-count-pipes text2))))
	      (format "{o:%i,%i}" (car frames) (cadr frames))
	      )
          )
	""
      )
    )
  )
)

(defun toyunda-current-line-frames (&optional noerror)
"Finds frames affected by the current line.
Returns a list of two elements, the first being the starting frame and
the second the end.
If the noerror argument is non-nil, returns nil instead of stopping and
emitting a message."
  (save-excursion
    (progn
      (beginning-of-line)
      (if (or (looking-at "^{\\([0-9]+\\)}{\\([0-9]+\\)}")
	      (looking-at "^{\\([0-9]+\\)}{}.+\n{\\([0-9]+\\)}"))
	  (list (string-to-int (match-string 1)) (string-to-int (match-string 2)))
	(if noerror
	    ()
	  (error "Not a valid toyunda line")
	)
      )
    )
  )
)

(defun toyunda-split-line ()
  "Splits a line of toyunda sub"
  (interactive)
  (save-excursion
    (let ((frames (toyunda-current-line-frames)))
      (beginning-of-line)
      (if (looking-at "^{\\([0-9]+\\)}{\\([0-9]*\\)}\\(.+\\)$")
	  (let ((endform (match-string 2))
		(text (match-string 3))
		(middleframe (/ (+ (car frames) (cadr frames)) 2)))
	  (end-of-line)
	  (push-mark)
	  (beginning-of-line)
	  (delete-region (point) (mark))
	  (insert (concat "{" (int-to-string (car frames)) "}{" (int-to-string middleframe) "}" text "\n"
			  "{" (int-to-string middleframe) "}{" endform "}" text))
	 )
	(message "Not a valid line")
      )
    )
  )
)


(defun toyunda-shift-fromframes-region-internal (start end amount)
  "This function increments the from frame by the amount given."
  (save-excursion
  (save-restriction
    (goto-char start)
    (while (re-search-forward "^{\\([0-9]+\\)}" end t)
      (let ((value (car (toyunda-current-line-frames))))
	(goto-char (match-beginning 1))
	(delete-region (match-beginning 1) (match-end 1))
	(insert (int-to-string (+ value amount)))))
)))

(defun toyunda-shift-toframes-region-internal (start end amount)
  "This function increments the to frame by the amount given."
  (save-excursion
  (save-restriction
    (goto-char start)
    (while (re-search-forward "^{[0-9]+}{\\([0-9]+\\)}" end t)
      (let ((value (cadr (toyunda-current-line-frames))))
	(goto-char (match-beginning 1))
	(delete-region (match-beginning 1) (match-end 1))
	(insert (int-to-string (+ value amount)))))
)))

(defun toyunda-shift-frames-region-internal (start end amount)
  "This function increments the region by the amount given."
  (save-excursion
  (save-restriction
    (goto-char start)
    (while (re-search-forward "^{\\([0-9]+\\)}{\\([0-9]*\\)}" end t)
      (let ((begin1 (match-beginning 1))
	    (end1 (match-end 1))
	    (begin2 (match-beginning 2))
	    (end2 (match-end 2))
	    (value (toyunda-current-line-frames)))
	(goto-char begin2)
	(delete-region begin2 end2)
	(insert (int-to-string (+ (cadr value) amount)))
	(goto-char begin1)
	(delete-region begin1 end1)
	(insert (int-to-string (+ (car value) amount))))
))))

(defun toyunda-shift-frames-region (start end &optional arg)
  (interactive "r\nP")
  (let ((amount (if (not arg)
		    1 (prefix-numeric-value arg))))
    (toyunda-shift-frames-region-internal start end amount)
))
(defun toyunda-shift-toframes-region (start end &optional arg)
  (interactive "r\nP")
  (let ((amount (if (not arg)
		    1 (prefix-numeric-value arg))))
    (toyunda-shift-toframes-region-internal start end amount)
))
(defun toyunda-shift-fromframes-region (start end &optional arg)
  (interactive "r\nP")
  (let ((amount (if (not arg)
		    1 (prefix-numeric-value arg))))
    (toyunda-shift-fromframes-region-internal start end amount)
))

(defun toyunda-insert-ampersands-region (start end)
  (interactive "r")
  (let ((case-fold-search nil))
    (replace-regexp "\\([sc]h[aeiou]\\|tsu\\|[bcdfghjklmnpqrstvwxyz][aeiou]?\\|[aeiou]\\)" "&\\1" nil start end)
))


(defvar toyunda-mode-hook nil)

(defvar toyunda-mode-map
  (let ((toyunda-mode-map (make-sparse-keymap))) ; Or make-sparse-keymap
    (define-key toyunda-mode-map "\M-p" 'toyunda-shift-fromframes-region)
    (define-key toyunda-mode-map "\M-n" 'toyunda-shift-toframes-region)
    (define-key toyunda-mode-map "\M-+" 'toyunda-shift-frames-region)
    (define-key toyunda-mode-map "\C-c/" 'toyunda-split-line)
    (define-key toyunda-mode-map "\C-cs" 'toyunda-add-size)
    (define-key toyunda-mode-map "\C-cc" 'toyunda-add-color)
    (define-key toyunda-mode-map "\C-co" 'toyunda-add-position)
    toyunda-mode-map)
  "Keymap for Toyunda major mode")

(add-to-list 'auto-mode-alist '("\\.toy\\'" . toyunda-mode))

(defvar toyunda-mode-syntax-table
  (let ((toyunda-mode-syntax-table (make-syntax-table)))
    (modify-syntax-entry ?, "." toyunda-mode-syntax-table)
    toyunda-mode-syntax-table)
  "Syntax table for toyunda-mode")

(defconst toyunda-font-lock-keywords-1
  (list
   '("^[^{\n%].*" . 'toyunda-comment-face)

   '("{\\([cosCOS]\\):[^}]*}" 1 'toyunda-keyword-face)

   '("^\\(%color\\)\s\\([0-9A-Fa-f]+\\)\s*\\([0-9A-Fa-f]+\\)?\s*\\([0-9A-Fa-f]+\\)?$" (1 'toyunda-size-face) (2 (toyunda-choose-color-face 2)) (3 (toyunda-choose-color-face 3) nil t) (4 (toyunda-choose-color-face 4) nil t))
   '("^\\(%set\\)\s\\([^\s]+\\)\s\\([^\s]+\\)$" (1 'toyunda-size-face) (2 'toyunda-frame-face) (3 'toyunda-keyword-face))
   '("^\\(%declare\\)\s\\([^\s]+\\)\s\\([^\s]+\\)$" (1 'toyunda-size-face) (2 'toyunda-frame-face) (3 'toyunda-keyword-face))
   '("^\\(%style\\)\s\\([^\s]+\\)$" (1 'toyunda-size-face) (2 'toyunda-keyword-face))

   '("\{[cC]:.\\([0-9A-Fa-f]+\\)\\(:.\\([0-9A-Fa-f]+\\)\\)?\}" (1 (toyunda-choose-color-face 1)) (3 (toyunda-choose-color-face 3) nil t))
   '("{[sS]:\\([0-9]+\\)\\(:\\([0-9]+\\)\\)?}" (1 'toyunda-size-face) (3 'toyunda-size-face nil t))
   '("{[oO]:\\(-?[0-9]+\\),\\(-?[0-9]+\\)\\(:\\(-?[0-9]+\\),\\(-?[0-9]+\\)\\)?}" (1 'toyunda-orientation-face) (2 'toyunda-orientation-face) (4 'toyunda-orientation-face nil t) (5 'toyunda-orientation-face nil t))
   '("�" . 'toyunda-bouboule-face)
   '("[:|}{]" . 'toyunda-symbol-face)

   '("{[oO]:-?[0-9]+\\(,\\)-?[0-9]+\\(:-?[0-9]+\\(,\\)-?[0-9]+\\)?}" (1 'toyunda-symbol-face) (3 'toyunda-symbol-face nil t))

   '("^{\\([0-9]+\\)}{\\([0-9]*\\)}" (1 'toyunda-frame-face) (2 'toyunda-frame-face))

   '("[^}]\\{40,\\}$" . 'toyunda-too-long-face)
   )
  "Syntax highlighting for toyunda-mode")

(defface toyunda-frame-face
  '((t :inherit font-lock-constant-face))
  "Face used to highlight Toyunda frame number expressions.")
(defvar toyunda-frame-face 'toyunda-frame-face)

(defface toyunda-color-face
  '((t :inherit font-lock-variable-name-face))
  "Face used to highlight Toyunda color expressions.")
(defvar toyunda-color-face 'toyunda-color-face)

(defface toyunda-size-face
  '((t :inherit font-lock-variable-name-face))
  "Face used to highlight Toyunda size expressions.")
(defvar toyunda-size-face 'toyunda-size-face)
(defface toyunda-orientation-face
  '((t :inherit font-lock-function-name-face))
  "Face used to highlight Toyunda orientation (position) expressions.")
(defvar toyunda-orientation-face 'toyunda-orientation-face)

(defface toyunda-symbol-face
  '((t :inherit font-lock-preprocessor-face))
  "Face used to highlight Toyunda symbols : :, |, ,, {, }.")
(defvar toyunda-symbol-face 'toyunda-symbol-face)

(defface toyunda-keyword-face
  '((t :inherit font-lock-keyword-face))
  "Face used to highlight Toyunda keywords : o, c, s.")
(defvar toyunda-keyword-face 'toyunda-keyword-face)

(defface toyunda-comment-face
  '((t :inherit font-lock-comment-face))
  "Face used to highlight Toyunda commented lines.")
(defvar toyunda-comment-face 'toyunda-comment-face)

(defface toyunda-bouboule-face
  '((t :inherit bold-italic))
  "Face used to highlight Toyunda bouboule.")
(defvar toyunda-bouboule-face 'toyunda-bouboule-face)

(defface toyunda-too-long-face
  '((t :inherit font-lock-warning-face))
  "Face used to highlight too long lines.")
(defvar toyunda-too-long-face 'toyunda-too-long-face)

(defvar toyunda-font-lock-keywords toyunda-font-lock-keywords-1
  "Default highlighting for toyunda-mode")

(define-derived-mode toyunda-mode fundamental-mode "Toyunda"
  "Major mode for editing Toyunda sub files."
  (set (make-local-variable 'font-lock-defaults) '(toyunda-font-lock-keywords))
  (set-face-background 'trailing-whitespace toyunda-trailing-whitespace-color)
;  (set (make-local-variable 'indent-line-function) 'toyunda-indent-line)
)

(defvar toyunda-trailing-whitespace-color "#303030")

(add-hook 'toyunda-mode-hooks '(setq show-trailing-whitespace t))

(setq comment-start "# ")

(provide 'toyunda-mode)
