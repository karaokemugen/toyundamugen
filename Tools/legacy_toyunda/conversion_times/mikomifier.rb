#!/usr/bin/ruby -w

FROM='00FFFF'
REACH='0000FF'
TO='007777'

class Point
  attr_reader :position, :length, :start, :end

  def initialize(line, text)
    if (!line.match(/\{(\d+)\}\{(\d+)\}\|+( *� *)/))
      raise "#$linenum : #{line} is not a valid bouboule line."
    end
    match = $3.chomp
    @start = $1.to_i
    @end = $2.to_i
    @text = text
    raise "Wrong length in line #$linenum. Not a bouboule line for \"#{text}.\"" if (@text.length != match.length)
    @position = match.index('�')
    (0...@position).each do |i| @text[i] = ' ' end
  end
  def to_s
    "Start : #@start, End : #@end, #{@position}th char of #@text\n"
  end

  def finish(i)
    (i...(@text.length)).each do |j|  @text[j] = ' ' end
  end

  def generate(s, e, comments)
    back = @end + 3
    back = e if (back > e)
    puts "{#{s-10}}{#{s}}#{comments}{c:$00#{FROM}:$FF#{FROM}}#@text"
    puts "{#{s}}{#{@start-2}}#{comments}{c:$FF#{FROM}}#@text"
    puts "{#{@start-2}}{#@end}#{comments}{c:$FF#{FROM}:$FF#{REACH}}#@text"
    puts "{#@end}{#{back}}#{comments}{c:$FF#{REACH}:$FF#{TO}}#@text"
    puts "{#{back}}{#{e}}#{comments}{c:$FF#{TO}}#@text" unless back == e
    puts "{#{e}}{#{e+10}}#{comments}{c:$FF#{TO}:$00#{TO}}#@text"
  end
end

class Line
  def initialize(line)
    if (!line.match(/\{(\d+)\}\{(\d+)\}(\|*(\{[csoCSO]:[^}]+\})*\|*)(.*)/))
      raise "#{line} is no valid toyunda line."
    end
    @start = $1.to_i
    @end = $2.to_i
    @defaultmodifiers = $3
    @text = $5
    @line = line
    @points = []
    @defaultmodifiers.gsub!(/\{c:[^}]+\}/, "")
    @bouboules = []
  end

  def to_s
    "Start : #@start, End : #@end ; Modifiers : #@defaultmodifiers\n" +
    "Text : #@text\n"
  end

  def add_point(l)
    @bouboules << l
    p = Point.new(l, @text.clone)
    @points.last.finish(p.position) unless nil == @points.last
    @points << p
  end

  def generate
    @points.each do |p|
      p.generate(@start, @end, @defaultmodifiers)
    end
    @bouboules.each do |l| print l end
  end
end

line = Line.new(gets)
$linenum = 1
while l = gets
  $linenum += 1
  line.add_point(l)
end
line.generate
