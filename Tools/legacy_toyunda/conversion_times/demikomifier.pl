#!/usr/bin/perl
# Sylvain "Skarsnik" Colinet <scolinet@gmail.com>
# This software is under the same licence as perl

use strict;
use Getopt::Long;
my $help;
GetOptions('help' => \$help);
if ($help) {
  print <<EOH;

demikomifier script

  simply use tike this:
    $0 mikomified_toyunda_file.txt > file.txt

  or with toy2lyr.pl
    $0 mikomified_toyunda_file.txt | ./toy2lyr.pl
  produce STDIN_gen files
EOH
  exit;
}
my $file = shift;

open TOY, $file or die "Can't open $file : $!";
while (<TOY>) {
  #take the stop time for start
  my ($curtext, $stop, $toy);
  my ($start, $text) = $_ =~ /^{\d+}{(\d+)}([[:print:]�]+)/;
  next unless $text;
  $text =~ s/{.*?}//g;
  #print "Text : $text\$\n";
  $curtext = <TOY>;
  while ($text ne $curtext) {
    my $line = <TOY>;
    $toy .= $line if ($line =~ /�/);
    ($stop, $curtext) = $line =~ /^{\d+}{(\d+)}([[:print:]�]+)/;
    $curtext =~ s/{.+?}//;
    #print "Curtext : $curtext\$\n";
    #print "Text : $text\$\n";
  }
  print "{$start}{$stop}$text\n";
  print $toy;
}
