:syntax clear
:syntax case match
:syntax	match	valnum /\d/ contained
:syntax keyword misc style color contained
:syntax keyword set set contained
:syntax match toy /&/
:syntax region nolyr start=/%/ end=/$/ contains=misc,set,valnum

hi link		misc	Keyword
hi link		set	String
hi link		valnum	Number
hi link		nolyr	Delimiter
hi link		toy	Identifier


let b:current_syntax = "lyr"
