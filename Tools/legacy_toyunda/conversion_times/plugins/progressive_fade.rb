##
## progressive_fade.rb
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##
require 'toyunda-lib'
include ToyundaLib

module ToyundaGen
  module Styles
    module SupportedStyles
      class Progressive_fade < Style
      	def init(vars)
	  vars.declare("color_from", Types::ColorArray.new(nil, "colors to fade from"))
	  vars.declare("color_to", Types::ColorArray.new(nil, "colors to fade to"))
          vars.declare("fade_from", Types::Int.new(0, "frame to fade from"))
          vars.declare("fade_to", Types::Int.new(0, "frame to fade to"))
	end

        def progress_fade(writer, line, frames, colorbefore, colorafter, str)
          if line.vars.fade_from < frames[0]
            if line.vars.fade_to < frames[0]
              writer.add(frames, colorafter, str)
            else
              if line.vars.fade_to < frames[1]
                c1 = Conversion.interpolate(frames[0], line.vars.fade_from, line.vars.fade_to, colorbefore.from, colorafter.from)
                c2 = Conversion.interpolate(line.vars.fade_to, frames[0], frames[1], colorafter.from, colorafter.to)
                c3 = colorafter.to
                writer.add([frames[0], line.vars.fade_to], ToyundaColor.new(c1[2..7], c2[2..7], c1[0..1], c2[0..1]), str)
                writer.add([line.vars.fade_to, frames[1]], ToyundaColor.new(c2[2..7], c3[2..7], c2[0..1], c3[0..1]), str)
              else
                c1 = Conversion.interpolate(frames[0], line.vars.fade_from, line.vars.fade_to, colorbefore.from, colorafter.from)
                c2 = Conversion.interpolate(frames[1], line.vars.fade_from, line.vars.fade_to, colorbefore.to, colorafter.to)
                writer.add(frames, ToyundaColor.new(c1[2..7], c2[2..7], c1[0..1], c2[0..1]), str)
              end
            end
          else
            if line.vars.fade_from > frames[1]
              writer.add(frames, colorbefore, str)
            else
              if line.vars.fade_to < frames[1]
                c1 = colorbefore.from
                c2 = Conversion.interpolate(line.vars.fade_from, frames[0], frames[1], colorbefore.from, colorbefore.to)
                c3 = Conversion.interpolate(line.vars.fade_to, frames[0], frames[1], colorafter.from, colorafter.to)
                c4 = colorafter.to
                writer.add([frames[0], line.vars.fade_from], ToyundaColor.new(c1[2..7], c2[2..7], c1[0..1], c2[0..1]), str)
                writer.add([line.vars.fade_from, line.vars.fade_to], ToyundaColor.new(c2[2..7], c3[2..7], c2[0..1], c3[0..1]), str)
                writer.add([line.vars.fade_to, frames[1]], ToyundaColor.new(c3[2..7], c4[2..7], c3[0..1], c4[0..1]), str)
              else
                c1 = colorbefore.from
                c2 = Conversion.interpolate(line.vars.fade_from, frames[0], frames[1], colorbefore.from, colorbefore.to)
                c3 = Conversion.interpolate(frames[1], line.vars.fade_from, line.vars.fade_to, colorbefore.to, colorafter.to)
                writer.add([frames[0], line.vars.fade_from], ToyundaColor.new(c1[2..7], c2[2..7], c1[0..1], c2[0..1]), str)
                writer.add([line.vars.fade_from, frames[1]], ToyundaColor.new(c2[2..7], c3[2..7], c2[0..1], c3[0..1]), str)
              end
            end
          end
        end

        def process_char(writer, line, i, fshift = i)
          syl = line.syl_from_char(i)
          color_from = line.vars.color_from
          color_to = line.vars.color_to
          until line.syls[syl].has_frames? or line.syls[syl].nil? do syl += 1 end
          if not line.syls[syl].nil? then
            start, stop = line.syls[syl].frames
            str = line.only_char(i)
            progress_fade(writer, line, [line.start - line.vars.fade_before + fshift, line.start + fshift], color_from[0].fade_in, color_to[0].fade_in, str)
            progress_fade(writer, line, [line.start + fshift, start], color_from[0], color_to[0], str)
            progress_fade(writer, line, [start, stop], ToyundaColor.new(color_from[1].bgr, color_from[2].bgr), ToyundaColor.new(color_to[1].bgr, color_to[2].bgr), str)
            progress_fade(writer, line, [stop, line.stop + fshift], color_from[2], color_to[2], str)
            progress_fade(writer, line, [line.stop + fshift, line.stop + line.vars.fade_after + fshift], color_from[2].fade_out, color_to[2].fade_out, str)
          end
        end
      end
    end
  end
end
