##
## skeleton.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id$
##
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles
      class Passing < Style

        def sum(i)
          if i == 0 then return 0 else return i + sum(i - 1) end
        end

        def process_syl(writer, line, i)

          x = ToyundaLib::Conversion.length_to_x(line.length)
          point1, point2 = line.frames
          start = point1 - line.vars.margin_before - line.vars.fade_before
          point0 = start + line.vars.fade_before
          point3 = point2 + line.vars.margin_after
          stop = point3 + line.vars.fade_after

          if line.has_frames?
            start.upto stop do |f|
              from, to = line.syls[i].frames
              color = case f
                      when (start...point0) then "{c:$" + ToyundaLib::Conversion.interpolate(f, start, point0, "00" + line.colors[0].bgr, "FF" + line.colors[0].bgr) + "}"
                      when (point0...point3)
                        if f < from
                          line.colors[0].to_toyunda_format
                        elsif f > to + 30
                          "{c:$" + "AF" + line.colors[2].bgr + "}"
                        elsif f > to
                          "{c:$" + ToyundaLib::Conversion.interpolate(f, to, to + 30, "FF" + line.colors[2].bgr, "AF" + line.colors[2].bgr) + "}"
                        else
                          "{c:$" + ToyundaLib::Conversion.interpolate(f, from, to, "FF" + line.colors[1].bgr, "FF" + line.colors[2].bgr) + "}"
                        end
                      when (point3..stop) then "{c:$" + ToyundaLib::Conversion.interpolate(f, point3, stop, "AF" + line.colors[2].bgr, "00" + line.colors[2].bgr) + "}"
                      end
              abs = case f
                    when (start...point1) then x + (stop - start) + 2*sum(point1 - f + 1)
                    when (point1...point2) then ToyundaLib::Conversion.interpolate(f, point1, point2, x + (stop - start), x - (stop - start)).to_i
                    when (point2..stop) then x - (stop - start) - 2*sum(f - point2 + 1)
                        end
              writer.add([f, f+1], ("|" * line.row) + color, "{o:#{abs},0}", line.only_syl(i))
              pos1 = ToyundaLib::Conversion.interpolate(from, point1, point2, x + (stop - start), x - (stop - start)).to_i
              pos2 = ToyundaLib::Conversion.interpolate(to, point1, point2, x + (stop - start), x - (stop - start)).to_i
              writer.add(line.syls[i].frames, ("|" * line.row) + "|", "{o:#{pos1},0:#{pos2},0}", line.only_cursor(line.syls[i]))
            end
          end

        end
      end
    end
  end
end
