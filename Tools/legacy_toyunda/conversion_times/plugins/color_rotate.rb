##
## skeleton.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id$
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##
require 'toyunda-lib'
include ToyundaLib

module ToyundaGen
  module Styles
    module SupportedStyles
      class Color_rotate < Style
      	def init(vars)
	  vars.declare("color_cycle", Types::String.new("", "list of color separate by coma"))
	  vars.declare("cycle_number", Types::Int.new(1, "The number of cycle (EXPERIMENTALE)"))
	  vars.declare("rotate_on_syl", Types::Bool.new(false, "Change the color of syl (NOT IMPLEMENTED)"))
	  vars.declare("frames_for_rotate", Types::String.new(nil, "Lists of time, separate my coma"))
	  @colors = []
	  @poscolor = 0
	  @first = true
	  @oldtime = nil
	end
	def do_cycle(line, writer, colors, start, offset, nbcolor)
	  cpt = 1 
	  colors.each do |x|
	    color = ToyundaColor.new(ColorName.to_bgr(x, line.vars.color_encoding))
	    writer.add([start + (offset / nbcolor) * (cpt - 1), start + (offset / nbcolor) * cpt], color, line)
	    cpt += 1
	  end
	end
        def process_line(writer, line)
	  if (line.vars.color_cycle == nil)
	    error "No color for the color_rotate style"
	  end
	  @colors = line.vars.color_cycle.split(',')
	  if (line.vars.frames_for_rotate == nil or (@oldtime == line.vars.frames_for_rotate and @oldtime != nil))
	    if (! line.vars.rotate_on_syl)
	      if (line.vars.cycle_number > 1)
	        warn "color_rotate : Be careful cycle_number don't have yet good implentation"
	      end
	      nbcolor = 0
	      offset = line.stop - line.start
	      nbcolor = @colors.size
	      do_cycle(line, writer, @colors, line.start, offset / line.vars.cycle_number, nbcolor)
              for i in 1..(line.vars.cycle_number - 1)
	        do_cycle(line, writer, @colors, 
	        line.start + offset / (line.vars.cycle_number - i + 1),
	        offset / line.vars.cycle_number, nbcolor)
	      end
	    else
	      color = ToyundaColor.new(ColorName.to_bgr(@colors[0], line.vars.color_encoding))
	      writer.add([line.start, line.frames[0]], color, line)
	    end
	  else
	    cpt = 1
	    times = line.vars.frames_for_rotate.split(',')
	    color = ToyundaColor.new(ColorName.to_bgr(@colors[0], line.vars.color_encoding))
	    writer.add([line.start, times[0].to_i], color, line)
	    for x in 1..(times.size - 1)
	      warn "a frame in frames_for_rotate is bigger than the end of line" if (times[x].to_i > line.stop)
	      color = ToyundaColor.new(ColorName.to_bgr(@colors[cpt], line.vars.color_encoding))
	      writer.add([times[x - 1].to_i, times[x].to_i], color, line)
	      cpt += 1
	      cpt = 0 if (cpt == @colors.size)
	    end
	    @first = false
	    @oldtime = line.vars.frames_for_rotate
	  end
	end
	def process_syl(writer, line, i)
	  if (line.vars.rotate_on_syl)
	    color = ToyundaColor.new(ColorName.to_bgr(@colors[@poscolor], line.vars.color_encoding))
	    writer.add(line.frames, color, line)
	    @poscolor += 1
	    @poscolor = 0 if (@poscolor == @colors.size)
	  end
	end
      end
    end
  end
end
