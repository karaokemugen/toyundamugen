##
## plain_with_fading.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id: plain_with_fading.rb 1.1 Tue, 21 Sep 2004 09:28:36 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles
      class Plain_with_fading < Style
        def process_line(writer, line)
          if line.has_frames? then
            color = line.colors[0]
            writer.add([line.start - line.vars.fade_before, line.start], color.fade_in, line)
            writer.add(line.frames, color, line)
            writer.add([line.stop, line.stop + line.vars.fade_after], color.fade_out, line)
          end
        end
      end
    end
  end
end
