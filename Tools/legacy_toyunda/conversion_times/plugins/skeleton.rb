##
## skeleton.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Fri Aug 27 18:13:58 2004 Olivier Leclant
## $Id: skeleton.rb 1.1 Tue, 21 Sep 2004 09:28:36 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

module ToyundaGen
  module Styles
    module SupportedStyles
      class Skeleton < Style
        def process_line(writer, line)
          error "killed by a skeleton at lvl 3"
          if line.has_frames?
            writer.add(line.frames, line.colors[0], line)
          end
        end
        def process_syl(writer, line, i)
          j = i
          until line.syls[j].has_frames? or line.syls[j].nil? do j += 1 end
          if not line.syls[j].nil? then
            start, stop = line.syls[j].frames
            str = line.only_syl(i)
            writer.add([line.stop, line.stop + line.vars.fade_after], line.colors[2].fade_out, str)
          end
          writer.add(line.frames, line.colors[0], line.only_syl(i)) if line.has_frames?
        end
        def process_char(writer, line, i)
          writer.add([frame - 1, frame], "{s:#{42},#{42}}", line.only_char(i))
        end
      end
    end
  end
end
