#!/usr/bin/env ruby
##
## toyunda_refactorizer.rb
## Login : <olivier@ryukros.all-3rd.net>
## Started on  Mon Sep 13 04:45:53 2004 Olivier Leclant
## $Id: toyunda_refactorizer.rb 1.2 Thu, 23 Sep 2004 01:51:27 +0200 olivier $
## 
## Copyright (C) 2004 Olivier Leclant
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
##

require "toyunda-lib"

if ARGV.include? "--help" or ARGV.empty? then
  $stderr.puts "Usage: #{$0} [IN_FILE] > [OUT_FILE]"
  exit!
end

ToyundaLib::ToyundaWriter.new.load(ARGV.shift).factorize!.dump

# c'�tait tr�s dur
