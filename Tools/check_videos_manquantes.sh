
KARAS=`ls ini/*.ini`
while read KARA
do
	VID=`cat "$KARA" | grep aviname | awk -F= {'print $2'}`	
	VID=`echo "$VID" | sed -e 's/^[ \t]*//'`
	VID=`echo "$VID" | sed -e 's/\r//'`
	if [ ! -e "videos/$VID" ]
	then 
		echo "$VID ($KARA)"
	fi
done <<< "$KARAS"
