#!/bin/bash
#Script de conversion des .ini de la base v2 en .inis de la nouvelle base Toyunda Mugen
#A lancer dans un dossier plein de .ini

ls *.ini >inis.txt

while read INI 
do
	NEWINI="$INI"
	
	# Partie recherche singer

	SINGER=""
	LYRICS=`grep -ai '^file' "$INI" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
	VIDEO=`grep -ai aviname "$INI" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
	ORIGIN=`grep -ai '^origin' "$INI" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
	YEAR=`grep -ai '^year' "$INI" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
	AUTHOR=`grep -ai '^author' "$INI" | awk -F= {'print $2'} | tr -d '\n' | tr -d '\r' | sed -e 's/^[[:space:]]*//'`
	echo "videofile=\"$VIDEO\"" >"$NEWINI"
	echo "subfile=\"$LYRICS\"" >>"$NEWINI"
	echo "year=\"$YEAR\"" >>"$NEWINI"

	TITLE=`grep -ai "^title" "$INI"`
	if [ $? == 0 ] 
	then
		echo $TITLE | grep " by " >/dev/null
		if [ $? == 0 ]
		then
			SINGER=`echo $TITLE | awk -F" by " {'print $NF'}`
		else
			SINGER=""
			echo $TITLE | grep "\["	>/dev/null
			if [ $? == 0 ]
			then
				SINGER=`echo $TITLE | awk -F"[" {'print $NF'}`
				SINGER=`echo $TITLE | awk -F"]" {'print $1'}`
			fi
		fi
		if [ "$SINGER" != "" ]
		then
			# On pense bien � retirer les \r car on ne l'a pas fait avant
			echo "singer=\"$SINGER\"" | sed 's/\r//g' >>"$NEWINI"
		fi
	else 
		echo "singer=\"$SINGER\"" | sed 's/\r//g' >>"$NEWINI"	
	fi

	echo "tags=\"\"" >>"$NEWINI"
	echo "songwriter=\"\"" >>"$NEWINI"
	echo "additional_languages=\"\"" >>"$NEWINI"
	echo "creator=\"\"" >>"$NEWINI"
	if [ "$AUTHOR" != "" ] 
	then
		echo "author=\"$AUTHOR\"" >>"$NEWINI"
	else
		echo "author=\"$ORIGIN\"" >>"$NEWINI"
	fi
	unix2dos "$NEWINI"
	NEWKARA=`basename "$NEWINI" .ini`
    git mv "$NEWINI" "$NEWKARA.kara"
done <inis.txt

rm inis.txt