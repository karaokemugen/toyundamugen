@echo off
cd /d %~dp0


if exist app\temp\player.pid (
    echo Player deja en cours d'execution. Quittez-la ou supprimez player.pid dans le dossier app\temp avant de relancer.
    pause
    exit /B
)

echo Lancement du player Toyunda Mugen
start "Toyunda-Akari (Player)" /d app\ php\php -f toyunda_mugen.php

echo Pour quitter, appuyez sur une touche :

pause
set /p PlayerPID=<app\temp\player.pid

REM Menage
del app\temp\player.pid

taskkill /F /PID %PlayerPID%