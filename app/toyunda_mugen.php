<?php
//Initialisation
include("config.php");
include("mkvextract.php");

$FichierPlaylist = $config['Playlist']['File'];
$Date = date("Y-m-d");
$FichierHistoPlaylist = 'liste_'.$Date.'.txt';

if(!file_exists('temp')) {
    mkdir('temp');
}

//D�tection de l'OS.
function getOS() {
	$OS = explode(" ",php_uname('S'));
	switch ($OS[0]) {
		case "Linux":
			return "Linux";
		break;
		case "Windows":
			return "Windows";
		break;
		case "Darwin":
			return "OSX";
		break;
	}
}

//Ecriture du num�ro de PID dans un fichier player.pid
$pid = getmypid();
if(file_exists('temp/player.pid'))
	unlink('temp/player.pid');
file_put_contents('temp/player.pid',$pid);

//Reset du log et ouverture du buffer.
if(file_exists("temp/log.txt"))
	unlink("temp/log.txt");

ob_start();

while (1) {
    //R�initialisation variables
    $ASS = "";
    $KaraFinal = ""; 
    $PlaylistKara = fopen($FichierPlaylist, 'r');
    $KaraRaw = fgets($PlaylistKara);
    $KaraRaw = preg_replace("/[\n\r]/","",$KaraRaw);
    $Kara = explode("|",$KaraRaw);
    $FichierKara = isset($Kara[0]) ? $Kara[0]:'';
    $Requester = isset($Kara[1]) ? $Kara[1]:'';

    fclose($PlaylistKara);
    
    //Analyse fichier ini

    if ( $FichierKara != "" && !file_exists("temp/pause") ) {
	include("config.php");
	print "Configuration actuelle : \n";
	print_r($config); 
	$FichierKara = iconv("UTF-8","ISO-8859-1","$FichierKara");

	print "\n\n\n";
	print "=============================================================================================\n\n";
        print "Lecture $FichierKara (demand� par $Requester)\n\n";
	print "=============================================================================================\n\n";
	$KaraArray = explode(" - ",$FichierKara);
	$KaraLangue = $KaraArray[0];
	$KaraSerie = $KaraArray[1];
	$KaraType = $KaraArray[2];
	$KaraTitre = $KaraArray[3];
	//On vire l'extension de KaraTitre pour �viter un joli .kara
	$KaraTitre = pathinfo($KaraTitre, PATHINFO_FILENAME); 

	//On r�cup�re le fichier de la vid�o et on enl�ve le retour � la ligne et d'autres joyeuset�s.
	print $config['Path']['Kara'].$FichierKara."\n";
	$Kara = parse_ini_file($config['Path']['Kara'].$FichierKara);
	$KaraVideo = $Kara['videofile'];
	var_dump($Kara);
	foreach($KaraVideo as $result) {
	    $KaraVideo = $result;
	}

	$KaraVideo = iconv("UTF-8","ISO-8859-1","$KaraVideo");	
	
	//Pareil avec les subs.
	$KaraSub = $Kara['subfile'];	
	$KaraSub = iconv("UTF-8","ISO-8859-1","$KaraSub");	
    $KaraVideo = $config['Path']['Video'].$KaraVideo;
    print "VIDEO : $KaraVideo \n";       
    if ($KaraSub != "dummy.ass") {
		$KaraSub = $config['Path']['Sub'].$KaraSub;
	}	
    print "LYRICS: $KaraSub \n";

	//On r�cup�re l'extension de la vid�o et du sub
    $KaraVideoInfo = pathinfo($KaraVideo);
    $KaraVideoExtension = $KaraVideoInfo['extension'];
	$KaraSubInfo = pathinfo($KaraSub);
    $KaraSubExtension = $KaraSubInfo['extension'];

	//C'est l� que tout se d�cide, on agit selon l'extension du sub car �a d�cide du launcher.
	unlink("temp/kara_temp.ass");
	switch ($KaraSubExtension) {
		case "vtt":
		case "srt":
		case "ass":
			// Avant de bidouiller le kara pour les credits, il faut qu'on fasse quelque chose si le .ass
			// utilis� est dummy.ass
			
			if ( $KaraSub == "dummy.ass" )
			{
				switch ($KaraVideoExtension) {
				case "mkv":	
					print "EXTRACTION DES SUBS\n";
					print "-------------------\n";
					extract_subtitle($KaraVideo);
					print "-------------------\n";
					$KaraSub = "temp/kara_extract.ass";
				break;
				case "mp4":
					print "EXTRACTION DES SUBS\n";
					print "-------------------\n";
					switch (getOS()) {
						case 'Windows':
							$cmd = $config['Player.Windows']['FFMpegPath'].$config['Player.Windows']['FFMpegBin']." -y -i ".$KaraVideo." temp/kara_extract.ass";
						break;
						case 'OSX':
							$cmd = $config['Player.OSX']['FFMpegPath'].$config['Player.OSX']['FFMpegBin']." -y -i ".$KaraVideo." temp/kara_extract.ass";
						break;
						case 'Linux':
							$cmd = $config['Player.Linux']['FFMpegPath'].$config['Player.Linux']['FFMpegBin']." -y -i ".$KaraVideo." temp/kara_extract.ass";
						break;
					}				
					system($cmd);
					print "-------------------\n";
					$KaraSub = "temp/kara_extract.ass";
				break;
				case "webm":
				case "avi":
					$KaraSub = "vide.ass";
				break;
				}
			} 
			// Avant de lancer le player, on va rajouter le titre en bas de l'�cran pendant 8 secondes
			echo "copy($KaraSub,\"temp/kara_temp.ass\")";	
			copy($KaraSub,"temp/kara_temp.ass");		
			// D'abord on recherche la cha�ne avec le style pour ajouter le style Cr�dits.	
			$Recherche = "Style: ";
			$ASS = file_get_contents("temp/kara_temp.ass");
			$ASS = explode("\n", $ASS);
			for ($ligne = 0; $ligne < count($ASS); $ligne++) { 
				if (strpos($ASS[$ligne], $Recherche) !== false) { 
					$LigneStyleDefault = $ligne;
				} 
			} 
			// Il nous faut la taille du playresX dans le fichier ass pour avoir une id�e 
			// de la taille de � mettre pour le style cr�dit. 
			// Si PLayresx ou y � 0 alors 15 �a suffit tr�s bien. Si c'est � 1280 par exemple, pour des
			// vid�os en 720p, alors il faut augmenter la taille du style � 40.
			$Recherche = "PlayResX";
			for ($ligne = 0; $ligne < count($ASS); $ligne++) { 
				if (strpos($ASS[$ligne], $Recherche) !== false) { 
					$ligneExplode = explode(" ",$ASS[$ligne]);
					$PlayResX = $ligneExplode[1];
				} 
			} 
			$PlayResX = (string)(isset($PlayResX) ? $PlayResX:'');
			print "PlayResX d�tect� : $PlayResX\n";
			switch ($PlayResX) {
				case 1920:
					$StyleCreditsTaille = 50;
					$StyleRequesterTaille = 25;
				break;
				case 1280:
					$StyleCreditsTaille = 40;
					$StyleRequesterTaille = 20;
				break;
				case "":
					//On a pas d�tect� de PlayResX, il faut donc se baser sur la taille du sub de la vid�o
					$Recherche = "Style: Default";
					for ($ligne = 0; $ligne < count($ASS); $ligne++) { 
						if (strpos($ASS[$ligne], $Recherche) !== false) { 
							$ligneExplode = explode(",",$ASS[$ligne]);
							$StyleDefaultTaille = $ligneExplode[2];
							//On r�cup�re la taille du style par d�faut
						}
					}
					$StyleCreditsTaille = $StyleDefaultTaille;
					$StyleRequesterTaille = 8;
				break;
				default:
					$StyleCreditsTaille = 15;
					$StyleRequesterTaille = 8;
				break;
			}			
			// On ajoute le style Cr�dits maintenant qu'on a le num�ro de la ligne de style default
			// Puis on ajoute le dialogue avec la s�rie et le titre � la fin du ASS. 
			// Comme le timing est de 0 � 8 secondes du d�but, il sera affich� anyway.
			array_splice($ASS,$LigneStyleDefault,0,["Style: Credits,Arial,$StyleCreditsTaille,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,-1,0,0,0,90,100,0,0,1,0.7,0,1,15,10,15,1"]);
			array_push($ASS,"Dialogue: 0,0:00:00.00,0:00:08.00,Credits,,0,0,0,,{\\fad(800,250)\\i1}$KaraSerie{\\i0}\\N{\\u0}$KaraType - $KaraTitre{\\u1}");

			// Si DisplayRequester est � 1 et que le antifloodtype = nickname alors on ajoute �a dans le .ass
			if ( $config['Playlist']['DisplayRequester'] == "1" && $config['Playlist']['AntiFloodType'] == 'Nickname' ) {
				if ($Requester == "") $Requester = "Inconnu";
				print "Demandé par $Requester\n";
				array_splice($ASS,$LigneStyleDefault,0,["Style: Pseudo,Arial,$StyleRequesterTaille,&H00FFFFFF,&H000000FF,&H64000000,&H64000000,0,0,0,0,100,100,0,0,3,3,0,3,15,10,15,1"]);
				array_push($ASS,"Dialogue: 0,0:00:00.00,0:00:08.00,Pseudo,,0,0,0,,{\\fad(800,250)\\i1}Demand� par{\\i0}\\N{\\u0}$Requester{\\u1}");
			}

			// On implose l'univers et on cr�e notre kara.ass
			$KaraFinal = implode("\n",$ASS);
			file_put_contents("temp/kara_temp.ass",$KaraFinal);
			
			print "Lancement MPlayer normal\n";
			switch (getOS()) {
				case Windows:
					$cmd = $config['Player.Windows']['MPlayerPath'].$config['Player.Windows']['MPlayerBin']." ".$config['Player.Windows']['MPlayerOptions']." --fs-screen=".$config['Player']['Screen']." --sub-file=\"temp\\kara_temp.ass\" \"".$KaraVideo."\"";
				break;
				case OSX:
					$cmd = $config['Player.OSX']['MPlayerPath'].$config['Player.OSX']['MPlayerBin']." ".$config['Player.OSX']['MPlayerOptions']." --fs-screen=".$config['Player']['Screen']." --sub-file=\"temp/kara_temp.ass\" \"".$KaraVideo."\"";
				break;
				case Linux:
					$cmd = $config['Player.Linux']['MPlayerPath'].$config['Player.Linux']['MPlayerBin']." ".$config['Player.Linux']['MPlayerOptions']." --fs-screen=".$config['Player']['Screen']." --sub-file=\"temp/kara_temp.ass\" \"".$KaraVideo."\"";
				break;
			}
			print "$cmd\n";
			system($cmd);
		break;
		case "txt":
	                print "Lancement MPlayer Toyunda \n";

			switch (getOS()) {
				case "Windows":
					//On remplace les / par des \
					$CheminSub = str_replace("/","\\",$config['Path']['Sub']);
					$CheminVideo = str_replace("/","\\",$config['Path']['Video']);
					$cmd = "cd ".$config['Player.Windows']['MPlayerToyundaPath']." & ".$config['Player.Windows']['MPlayerToyundaBin']." ".$config['Player.Windows']['MPlayerToyundaOptions']." -font font/font.desc -sub \"..\\..\\..\\data\\lyrics\\".$KaraSub."\" \"..\\..\\..\\data\\videos\\".$KaraVideo."\"";
				break;
				case "OSX":
					print "Erreur : Ne fonctionne pas sous OSX.\n";
				break;
				case "Linux":
					$cmd = $config['Player.Linux']['MPlayerToyundaPath'].$config['Player.Linux']['MPlayerToyundaBin']." ".$config['Player.Linux']['MPlayerToyundaOptions']." -sub \"".$config['Path']['Sub'].$KaraSub."\" \"".$config['Path']['Video'].$KaraVideo."\"";
				break;
			}

			print "$cmd\n";
			system($cmd);
		break;            
        }
        //Suppression premi�re ligne de la playlist
        $PlaylistKara = file($FichierPlaylist, FILE_IGNORE_NEW_LINES);
	$PremiereLigne = array_shift($PlaylistKara);
        $content=($PlaylistKara)?implode("\r\n", $PlaylistKara)."\r\n":'';
	file_put_contents($FichierPlaylist, $content);	
        file_put_contents('temp/log.txt',ob_get_contents(),FILE_APPEND);    
        
	//Ajout de la chanson dans l'historique des playlists
	if (!file_exists($config['Path']['PlaylistHistory'])) {
	    mkdir($config['Path']['PlaylistHistory']);
	}
	file_put_contents("historique_playlists/".$FichierHistoPlaylist, $FichierKara.PHP_EOL,FILE_APPEND);
    } 
    
    //Dodo.
    sleep(1);
    
}

 ?>
