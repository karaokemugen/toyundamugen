<?php
/**
 * @author Gica <xprt64@gmail.com>

/**
 * Extract subtitle from all mkv files, recursive into dir
 * @param string $dir
 */
function parse_dir_recursive($dir)
{
	$files = scandir($dir);
	
	foreach($files as $file)
	{
		if('.' == $file || '..' == $file)
			continue;
		
		$path	=	$dir . DIRECTORY_SEPARATOR . $file;
		
		if(is_dir($path))
			parse_dir_recursive($path);
		else if(preg_match("#\.mkv$#ims", $file))
			extract_subtitle($path);
	}
}

/**
 * Extract subtitle from file and place it under the same name as the filename,
 * with the srt extension
 * @param string $file Full mkv file path
 * @return void
 */
function extract_subtitle($file)
{
	pecho("extract_subtitle($file)");
	
	$found_language	=	'';
	
	$track_id	=	find_track_id($file, $found_language);

	if(false === $track_id)
	{
		pecho("no subtitle found!\n");
		return;
	}

	pecho("Found track $track_id, language $found_language");

	extract_subtitle_track($file, $track_id);
}

/**
 * Returns the track id of the subtitle. It searches rum subtitles, then eng, 
 * then the first if no rum or eng subtitles are found
 * @param string $file Full path to mkv file
 * @param string $found_language Optional, the language
 * @return int
 */
function find_track_id($file, &$found_language = null)
{

	switch (getOS()) {
				case "Windows":
					$cmd = "tools\\mkvmerge.exe -I \"$file\"";
				break;
				case "OSX":
					$cmd = "tools/mkvmerge.osx -I \"$file\"";
				break;
				case "Linux":
					$cmd = "/usr/bin/mkvmerge -I \"$file\"";
				break;
	}

	$output	=	array();
	
	$ret	=	0;
	
	exec($cmd, $output, $ret);
	
	if($ret)
		die("command $cmd failed: $ret (" . implode("\n", $output) . ")");
	
	$subtitle_tracks	=	array();
	
	foreach($output as $line)
	{
		if(preg_match("#Track ID (?P<id>\d+)\:\s*subtitles(:?.*?)language\:(?P<lang>[a-z]+)#ims", $line, $m))
		{
			$subtitle_tracks[$m['id']]	=	$m['lang'];
			
			//daca suntem norocosi si avem subtitrare in limba romana
			if($m['lang'] == 'rum' || $m['lang'] == 'rom')
			{
				$found_language	=	$m['lang'];
				return	$m['id'];
			}
		}
	}
	
	if(!$subtitle_tracks)
	{
		pecho("no subtitle tracks found!\n");
		return;
	}
	
	//cautam subtitrarea in limba engleza
	foreach($subtitle_tracks as $id => $lang)
	{
		if('eng' === $lang)
		{
			$found_language	=	$lang;
			return $id;
		}
	}
	
	//returnam prima subtitrare gasita
	$found_language	=	reset($subtitle_tracks);
	
	return reset(array_keys($subtitle_tracks));
}

/**
 * Extract subtitle with track id from mkv file
 * @param string $file Full path to mkv file
 * @param int $track_id Track id of the subtitle
 */
function extract_subtitle_track($file, $track_id)
{
	$subtitle_path	= "temp/kara_extract.ass";	

	if(is_file($subtitle_path))
	{
		unlink($subtitle_path);
	}
	
	if($track_id === '' || $track_id === null)
	{
		pecho("null track id");
		return;
	}
	
	switch (getOS()) {
				case "Windows":
					$cmd = 'tools\\mkvextract.exe tracks "' . $file . '" ' . $track_id . ':"' . $subtitle_path . '"';
				break;
				case "OSX":
					$cmd = 'tools/mkvextract.osx tracks "' . $file . '" ' . $track_id . ':"' . $subtitle_path . '"';
				break;
				case "Linux":
					$cmd = '/usr/bin/mkvextract tracks "' . $file . '" ' . $track_id . ':"' . $subtitle_path . '"';
				break;
	}
	
	pecho($cmd);
	
	exec($cmd, $output, $ret);
	
	if($ret)
	{
		pecho("command $cmd failed: $ret (" . implode("\n", $output) . ")");
		return;
	}
}

/**
 * Print debug messages
 * @param mixed $s
 */
function pecho($s)
{
	print_r($s);
	echo "\n";
}