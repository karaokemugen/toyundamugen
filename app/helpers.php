<?php

// return kara position in playlist (numeric index as integer)
// return false if kara not found
/* playlist formated as
nom du fichier kara.ini|requester name
nom du fichier kara.ini|requester name
nom du fichier kara.ini|requester name
nom du fichier kara.ini|requester name
nom du fichier kara.ini|requester name
*/
function search_kara_in_playlist($playlist,$kara)
{
    $pos=false;
    foreach($playlist as $k=>$v)
    {
        if(strpos($v,$kara.'|')===0) // search sequence like "nom du fichier kara.ini|" at position 0 in text line
        {
            $pos = $k;
            break;
        }
    }
    return $pos;
}