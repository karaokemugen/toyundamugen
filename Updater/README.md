# Configuration du rsync c�t� client

Modifiez les fichiers .cmd "Mettre � jour Toyunda Mugen".

* Mettre � jour Toyunda Mugen.cmd : mise � jour de l'ensemble du dossier (rsync serveur -> local)
* Mettre � jour Toyunda Mugen (app uniquement).cmd : mise � jour de l'application Toyunda Mugen uniquement, sans la base (rsync serveur -> local)
* Mettre � jour Toyunda Mugen (serveur) : mise � jour de l'ensemble du dossier (rsync local -> serveur)

Le dernier script se connecte � la ressource rsync ToyundaRW, tandis que les autres � la ressource Toyunda.

Exemples de rsyncd.conf :

```
[toyunda]
path = /home/aterizak/Toyunda
uid = aterizak
gid = vip
list = false
use chroot = false
read only = true
auth users = user1 user2 user3
secrets file = /etc/rsyncd.secret

[toyundaRW]
path = /home/aterizak/Toyunda
uid = aterizak
gid = vip
list = false
use chroot = false
read only = false
auth users = user1
secrets file = /etc/rsyncd.secret
```

Le rsyncd.secret devrait �tre au format :

```
user1:password
```

Avec une entr�e par ligne.