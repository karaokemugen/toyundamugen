@ECHO OFF
SETLOCAL
SET CWRSYNCHOME=.
SET HOME=.
SET CWOLDPATH=%PATH%
SET PATH=%CWRSYNCHOME%\BIN;%PATH%
SET HOST=shelter.mahoro-net.org
SET RSYNCRESSOURCE=toyundaRW

echo Toyunda Mugen
echo =============
echo.
echo RESERVE AUX ADMINS
echo. 
echo Entrez le login et le mot de passe pour mettre � jour la Toyunda Mugen du serveur
echo Ne partagez pas vos login et mots de passe.
echo.
SET /p LOGIN="Login: "

rsync -ruvh --progress --delete-during --exclude="Mettre � jour Toyunda Mugen (serveur).cmd" --exclude=".DS_Store" --exclude=".git" --exclude="config.ini" --exclude="Thumbs.db" --exclude="ef9c9ad8cc5857eb63cb3660bc8bd202-le64.cache-7" --exclude="/dev" --exclude="temp/" --exclude="historique_playlists" --exclude="/cygdrive" --exclude="/proc" --exclude=".svn" --exclude=".gitignore" .. %LOGIN%@%HOST%::%RSYNCRESSOURCE%
pause