#!/bin/bash

echo Toyunda Mugen
echo =============
echo
echo Entrez le login et le mot de passe pour mettre a jour votre Toyunda Mugen
echo Ne partagez pas vos login et mots de passe.
echo "Le mot de passe ne s'affiche pas, ceci n'est pas un bug, n'essayez pas de regler votre ecran."
echo 
echo Tapez votre login:
read rsynclogin

rsync -ruvh --progress --delete-during --exclude="blacklist.txt" --exclude="historique_playlists" --exclude="temp/" --exclude="app/config.ini" --exclude=".git" --exclude=".svn" $rsynclogin@shelter.mahoro-net.org::toyunda .
