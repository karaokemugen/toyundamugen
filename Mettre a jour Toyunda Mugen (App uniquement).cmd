@ECHO OFF
SETLOCAL
SET CWRSYNCHOME=.\Updater
SET HOME=.\Updater
SET CWOLDPATH=%PATH%
SET PATH=%CWRSYNCHOME%\BIN;%PATH%
SET HOST=shelter.mahoro-net.org
SET RSYNCRESSOURCE=toyunda

echo Toyunda Mugen
echo =============
echo Mise a jour de l'app uniquement! Les karas ne seront pas touch�s.
echo.
echo Entrez le login et le mot de passe pour mettre � jour votre Toyunda Mugen
echo Ne partagez pas vos login et mots de passe.
echo.
echo Le mot de passe ne s'affiche pas, ceci n'est pas un bug, n'essayez pas de regler votre �cran.
echo.
SET /p LOGIN="Login: "

Updater\rsync -ruvh --progress --delete-during --exclude="/dev" --exclude="/cygdrive" --exclude="/proc" --exclude="blacklist.txt" --exclude="app/config.ini" --exclude="temp/" --exclude="app/data" --exclude=".svn" --exclude=".git" %LOGIN%@%HOST%::%RSYNCRESSOURCE% .
pause