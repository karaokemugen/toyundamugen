﻿# TOYUNDA MUGEN


Bienvenue dans Toyunda Mugen. 

Toyunda Mugen est un système de gestion de playlist de karaoke vidéo. Il se présente sous la forme d'une appli web et d'un daemon. L'appli web permet aux gens d'ajouter des chansons à la playlist, le daemon tourne et lance ces chansons sur l'ordinateur qui projette les vidéos.
Il fonctionne comme un karaoké à la japonaise où chacun peut ajouter des chansons à la suite de la playlist.

## Principe

Il y a deux modules :
- Un serveur web sur lequel les utilisateurs du karaoke dans la pièce se connectent (cela implique qu'ils soient sur le même réseau wifi) et ajoutent des chansons à une playlist
- Un daemon en PHP qui lit la playlist en continu et joue les chansons qui y sont ajoutées au fur et à mesure. Le daemon lance le player vidéo nécessaire pour le karaoke (selon le type de fichier karaoke) 

Les fichiers doivent être placés dans un répertoire app/data. Voir le fichier de configuration (config.ini) pour plus d'informations.

## Téléchargement

Vous pouvez récupérer une version spécifique sous forme de zip via le bouton de téléchargement sous l'avatar et la description du projet.

Si vous préférez utiliser git, attention :

Ce dépôt git utilise git-lfs pour gérer les fichiers binaires. Pour plus d'informations : https://git-lfs.github.com/

Si vous n'installez pas git-lfs les fichiers binaires ne seront pas téléchargés depuis le dépôt, un fichier "ressource" sera téléchargé à sa place et vous ne pourrez pas utiliser Toyunda Mugen tel quel.

Si vous récupérez le dépôt en ligne de commande, pensez à activer un cache d'authentification :

* Sous Windows :
```git config --global credential.helper wincred```
* Sous Linux/OSX :
```git config --global credential.helper cache```

## Pré-requis systèmes

- Windows :
    - Windows 7 minimum requis. Système 64 Bits requis.
- macOS : 
    - 10.9 minimum requis. 
- Linux : 
    - Votre distribution doit avoir installé :
    	- PHP (avec mbstring et gd)
    	- mpv
        - mplayer-toyunda (0.99 Lichking - voir docs/mplayer-toyunda.md)
    	- mkvtoolnix
    	- ffmpeg
    	- rsync (pour le système de mise à jour, si utilisé.)
    
## Langages

Toyunda Mugen est écrit en PHP, avec quelques scripts bash pour les scripts de maintenance ou à usage unique, et deux-trois trucs en batch Windows pour les launchers.

## Fonctionnalités :

- Gère vidéos seules ou vidéos + sous-titres
- Gère les formats de time ass/txt(Toyunda)/srt/vtt
- Permet de passer une chanson
- Ajout de chanson après celle en cours
- Mettre le karaoke en pause ou le relancer
- Shuffle de la playlist
- Ajout de la base entière dans la playlist
- Arrêt complet depuis l'interface web
- Interface mobile (incomplète) et web
- Affiche le titre du karaoké au début de la vidéo.
- Système de mise à jour rsync intégré si vous avez votre propre base de karaokés sur un serveur dédié.

## Comment ça fonctionne

* Assurez-vous d'avoir un dossier "data" dans "app" avec les dossiers suivants :
	* ini
	* lyrics
	* videos

* Placez des karaoké à l'intérieur. Voir https://lab.shelter.moe/toyundamugen/times

* Sous Windows :
    * Lancez "Démarrer Toyunda Mugen" 
    	* Un serveur Web (Haruka) se lance sur http://<votre_adresse_IP_locale>:1337
    	* Un player (Akari) se lance et attend que le fichier liste.txt dans le dossier "app/temp" se remplisse. 

* Sous OSX : 
    * Lancez StartToyundaMugen.sh

* Sous Linux :
    * Ubuntu :
        * Lancez installDependancesUbuntu.sh
        * Lancez StartToyundaMugen.sh

Connectez-vous avec un navigateur web (sur votre machine ou depuis une autre comme un smartphone ou tablette) à http://<votre_adresse_IP_locale:1337
Chaque ajout à la playlist est pris en compte immédiatement.

Sur le clavier de la machine lançant Toyunda Mugen :
* ESC, q ou ALT+F4 : quitter le kara en cours (ça passe à la chanson suivante)
* Flèche gauche et droite : avancer un peu ou reculer dans le kara
* Flèche haut et bas : avancer ou reculer plus vite
* ESPACE : pause

Vous pouvez également lancer les deux éléments de Toyunda Mugen individuellement :
* Uniquement le Player si vous avez déjà préparé votre liste.txt et que vous ne voulez pas que des gens ajoutent des chansons en cours de lecture.
* Uniquement la Webapp si vous ne voulez pas démarrer le karaoke tout de suite et que vous souhaitez que les gens ajoutent des chansons à la playlist sans qu'elle ne démarre immédiatement.

### Editer la playlist depuis la Webapp
* Allez dans la playlist (bouton en haut tout à gauche)
* Cliquez sur le bouton clef (Admin) en bas à droite
* Le mot de passe par défaut est shami (modifiable dans app/config.ini)
* Vous pouvez : 
	* Retirer des éléments de la playlist
	* Passer la chanson en cours
	* Randomizer la playlist
	* Ajouter tout le contenu de la base dans la playlist
	* Mettre en pause le karaoké
	* Arrêter Toyunda Mugen
	* Sur la page d'ajout de chansons, vous pouvez décider d'ajouter la chanson juste derrière la chanson en cours.

### Editer la playlist en temps réel via un autre ordinateur.
* Créez un partage réseau du dossier app pour pouvoir éditer temp/liste.txt depuis un autre ordinateur.
* Utilisez un éditeur comme EditPad Lite ou Notepad++ qui recharge la liste à chaque fois qu'elle est modifiée.
* Ouvrez en parallèle liste_all.txt (créez-le si besoin avec l'outil Creer Liste All dans le dossier Tools) pour pouvoir ajouter facilement des chansons en loosedé. Copiez-collez les chansons que vous voulez ajouter depuis liste_all.txt vers liste.txt

### Quitter le karaoké
* Utilisez le bouton "Quitter" 
* Assurez-vous que Toyunda Akari et Toyunda Haruka sont bien fermées.

### Blacklist

Vous pouvez ajouter dans le fichier app/temp/blacklist.txt les karas que vous ne souhaitez pas voir apparaître dans la liste.

Il faut une entrée par kara qu'on souhaite blacklister, avec le .ini à la fin de chaque entrée.

### Utilitaires

Il y a des outils divers et variés dans Tools.

### Comment ajouter vos propres karas sans les envoyer.

C'est possible mais c'est mieux de partager ! 

* Un fichier .ini dans le dossier app/data/ini 
* Un fichier .txt (Toyunda) / .ass / .srt dans app/data/lyrics. Ce fichier n'est pas nécessaire si votre vidéo contient des sous-titres déjà dans le container (comme dans les mkv)
* Un fichier .mkv/.avi/.mp4/.webm... dans app/data/videos

Consultez le README.md du dépôt des times pour plus d'informations sur le format.
https://lab.shelter.moe/toyundamugen/times

## Comment participer au code

Lisez CONTRIBUTING.md

## Envoyer vos propres karaokés pour validation

* Le plus simple c'est d'aller là : http://mei.do/toyundamugen
	* Ceci m'envoie votre kara pour que je le valide et l'ajoute à la base : https://lab.shelter.moe/toyundamugen/times
* Vous pouvez également contribuer vous-même à cette base si vous préférez le faire. Pour la vidéo, merci d'indiquer un lien où la récupérer dans votre commit.
