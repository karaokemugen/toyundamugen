@echo off


REM récuperation de l'IP
for /f "tokens=1-2 delims=:" %%a in ('ipconfig^|find "IPv4"') do set ip=%%b
set ip=%ip:~1%

cd /d %~dp0

TITLE Toyunda-Mugen (Startup)

if exist app\temp\webapp.pid (
    echo Webapp deja en cours d'execution. Quittez-la ou supprimez webapp.pid dans le dossier app\temp avant de relancer.
    pause
    exit /B
)

if exist app\temp\player.pid (
    echo Player deja en cours d'execution. Quittez-la ou supprimez player.pid dans le dossier app\temp avant de relancer.
    pause
    exit /B
)

start "Toyunda-Haruka (Webapp)" /min php\php.exe -S 0.0.0.0:1337 -t app
start "Toyunda-Akari (Player)" /min /d app\ php\php.exe -f toyunda_mugen.php


echo ======================================================
echo POUR VOUS CONNECTER A L'INTERFACE WEB :   
echo              http://%ip%:1337                
echo ======================================================
echo.
 

REM Génération du QR-Code:

php\php.exe -f app\qrcode-generator.php "http://%ip%:1337"

echo =========================================================
echo ==== APPUYEZ SUR ENTER POUR QUITTER TOYUNDA MUGEN    ==== 
echo ==== (Windows ment! Ca quitte et ca ne continue pas! ====
echo =========================================================


start qrcode.png

pause

del qrcode.png

REM On va poke la webapp pour la reveiller et lui faire cracher son PID
REM Au cas ou l'utilisateur n'est jamais alle dessus.
php\php.exe -f app\poke_webapp.php

set /p WebappPID=<app\temp\webapp.pid
set /p PlayerPID=<app\temp\player.pid

REM Menage
del app\temp\webapp.pid
del app\temp\player.pid

taskkill /F /PID %WebappPID% /PID %PlayerPID%