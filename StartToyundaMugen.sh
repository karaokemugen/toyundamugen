#!/bin/bash

#Récupérer l'adresse IP

case `uname -s` in
	"Darwin")
		IP=`ipconfig getifaddr en0`
	;;
	"Linux")
		IP=`ip route get 1 | awk '{print $NF;exit}'`
	;;
esac

php -S 0.0.0.0:1337 -t app &>/dev/null &disown
cd app
php -f toyunda_mugen.php &>/dev/null &disown
cd ..

php -f app/qrcode-generator.php "http://$IP:1337"

#Afficher l'adresse IP pour se connecter
echo "======================================"
echo "http://$IP:1337"
echo "======================================"

#Afficher le QR-Code :

case `uname -s` in
	"Darwin")
		open qrcode.png
	;;
	"Linux")
		xdg-open qrcode.png
	;;
esac

php -f app/poke_webapp.php

WebappPID=`cat app/temp/webapp.pid`
PlayerPID=`cat app/temp/player.pid`

read -n1 -r -p "Appuyez sur une touche pour quitter..." key

#REM Menage
rm -f app/temp/webapp.pid
rm -f app/temp/player.pid

kill -9 $WebappPID
kill -9 $PlayerPID

